The Vault Tree DSL provides keywords that allow you to craft the behavior of your contract in useful ways.

Keywords are expressed with all capital letters and can take **Vault Ids** as arguments.
