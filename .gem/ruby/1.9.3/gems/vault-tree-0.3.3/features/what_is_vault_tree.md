Vault Tree is a software framework that makes it easy to author and execute **Distributed Cryptographic Contracts.**

These are sort of like [Smart Contracts], but designed for for web developers building applications against the Bitcoin Block Chain.

[Smart Contracts]: http://en.wikipedia.org/wiki/Smart_contract

## Background

Contracts are fundemental building blocks of our modern economy. They are simply voluntary and structured aggreements between two or more parties.

There are many examples that we are already familiar with from our daily lives such as employment contracts and rental agreements. Also, the sophisticated investment instruments that drive our modern financial system are just examples of standardized contracts.

It's been know for quite a while that well structured contracts can be thought of as a computer programs. If we bring cutting edge [cryptographic libraries], and distributed virtual currencies such as [Bitcoin] into the picture, can we change the way we view the problem of **contract enforcement**?

You can think of Vault Tree as a collection of tools that will help web developers, businesses, and online communities explore a new way of thinking about contracts.

[cryptographic libraries]: http://en.wikipedia.org/wiki/Cryptography
[Bitcoin]: http://bitcoin.org/en/ 
