Contracts become pointless if their terms cannot be effectly **enforced**. This enforcement problem is handled differently depenending on the nature of the contract and the parties involved.

One of the most primative, widespread, and **decentalized** enforcement mechanisms in the world is the **Network of Trust**. Breach of contract, even if no legal action is taken, undermines the reputation of of the offending party. This can translate into substantial costs and acts as an incentive toward honorable conduct.

In many cases however, a network of trust is an insufficient mechanism. When parties expect a more explict enforcement of their contract, they often turn to **centralized** solutions. Examples could include:
  * Legal action within a justice system
  * Private third party arbitration

This type of enforcement works well when the central enforcing authority is:
  * Agreed to by all contract participants
  * Competent and Efficient
  * Trustworthy

Centralized enforcement is less effective or impossible when:
  * Participants are distributed globally
  * Business is conducted with a cutting edge internet finacial system that is ahead of established legal precedent
  * Good people are are trying to conduct honorable business under extremely corrupt or incompetent legal systems



