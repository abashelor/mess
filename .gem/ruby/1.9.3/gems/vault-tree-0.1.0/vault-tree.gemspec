# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'vault-tree/version'

Gem::Specification.new do |spec|
  spec.name          = "vault-tree"
  spec.version       = VaultTree::VERSION
  spec.authors       = ["Andy Bashelor"]
  spec.email         = ["abash1212@gmail.com"]
  spec.description   = %q{Vault Tree is a Ruby library for executing distributed cryptographic contracts.}
  spec.summary       = %q{The Self Enforcing Contract}
  spec.homepage      = ""
  spec.license       = "MIT"

  spec.files         = `git ls-files`.split($/)
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.test_files    = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths = ["lib"]

  spec.add_dependency "rbnacl", "1.1.0"
  spec.add_dependency "require_all"
  spec.add_development_dependency "bundler", "~> 1.3"
  spec.add_development_dependency "rspec"
  spec.add_development_dependency "cucumber"
end
