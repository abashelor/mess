# require_relative - Ruby > 1.9 method
# require_rel - 'gem require_all' helper method to require all relative

require_relative 'vault-tree/config/dependencies'
require_relative 'vault-tree/config/path_helpers'
require_relative 'vault-tree/config/string'
require_relative 'vault-tree/config/lib'
