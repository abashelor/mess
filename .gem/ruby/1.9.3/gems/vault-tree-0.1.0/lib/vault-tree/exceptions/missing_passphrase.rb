module VaultTree
  module Exceptions
    class MissingPassphrase < VaultTreeException
    end
  end
end
