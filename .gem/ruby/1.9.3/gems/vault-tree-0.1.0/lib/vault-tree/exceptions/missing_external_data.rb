module VaultTree
  module Exceptions
    class MissingExternalData < VaultTreeException
    end
  end
end
