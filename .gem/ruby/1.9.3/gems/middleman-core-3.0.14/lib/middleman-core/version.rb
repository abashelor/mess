module Middleman
  # Current Version
  # @return [String]
  VERSION = '3.0.14' unless const_defined?(:VERSION)
end
