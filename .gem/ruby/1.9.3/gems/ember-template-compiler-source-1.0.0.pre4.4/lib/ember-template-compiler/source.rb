module EmberTemplateCompiler
  module Source
    def self.bundled_path
      File.expand_path("../../../dist/ember-template-compiler.js", __FILE__)
    end

    def self.prod_bundled_path
      File.expand_path("../../../dist/ember-template-compiler.prod.js", __FILE__)
    end

    def self.min_bundled_path
      File.expand_path("../../../dist/ember-template-compiler.min.js", __FILE__)
    end
  end
end
