Feature: Decryption Key
  
Decryption keys are asymmetric private keys.

```javascript
"fill_with": "DECRYPTION_KEY",
```

When establishing a public private key pair, you need to generate the decryption
key first and reference its vault when building the associated public key.
