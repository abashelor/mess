### Contracts as Decision Trees

Contracts can be modeled as a [Decision Tree]. Intuitively this means that:

* The set of contract events and final outcomes for all parties involved can be represented as a [Directed Graph].
* Often, the graph will take the form of a [Tree].
* **Events** are represented as nodes in the graph, **Outcomes** are events that correspond to [Leaf Nodes].
* Once a contract has been **Negotiated**, contract execution proceeds down the graph beginning at the [Root Node].
* Edges in the graph correspond to real world conditions that must be met before making a valid trasition between two events.
* **Well Written** contracts, in the eyes of all parties, correspond to well structured graphs.

[Directed Graph]: http://en.wikipedia.org/wiki/Directed_graph
[Tree]: http://en.wikipedia.org/wiki/Tree_%28graph_theory%29 
[Decision Tree]: http://en.wikipedia.org/wiki/Decision_tree
[Leaf Nodes]: http://en.wikipedia.org/wiki/Tree_%28data_structure%29#Terminology
[Root Node]: http://en.wikipedia.org/wiki/Tree_%28data_structure%29#Terminology
