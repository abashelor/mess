## 0.3.4

* Bug Fix in Exception Handling
* Eliminate File missing error when execption is thrown

## 0.3.3

* Update to RbNaCl 2.0.
* Major changes to crypto lib interface
* Now Require Libsodium >= 0.4.3

## 0.2.3

* Reorganize features dir to work with Relish
* Modify some Cucumber features to explicitly take a hard coded contract

## 0.2.1

* First pass implementation of GENERATED_SHAMIR_KEY and ASSEMBLED_SHAMIR_KEY
* Using version 0.3 of https://github.com/grempe/secretsharing
