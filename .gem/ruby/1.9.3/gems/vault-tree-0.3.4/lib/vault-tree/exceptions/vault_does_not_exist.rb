module VaultTree
  module Exceptions
    class VaultDoesNotExist < VaultTreeException
    end
  end
end
