module VaultTree
  module Exceptions
    class FailedUnlockAttempt < VaultTreeException
    end
  end
end
