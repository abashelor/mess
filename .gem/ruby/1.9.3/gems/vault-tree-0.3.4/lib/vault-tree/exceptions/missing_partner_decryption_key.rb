module VaultTree
  module Exceptions
    class MissingPartnerDecryptionKey < VaultTreeException
    end
  end
end
