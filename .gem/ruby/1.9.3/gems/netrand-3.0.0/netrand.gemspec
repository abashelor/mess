# coding: utf-8

Gem::Specification.new do |s| 
  s.name        = "netrand"
  s.summary     = "Generate true random numbers using random.org"
  s.description = "Wrapper around Random.org random number and string generator."
  s.requirements=  [ 'Working internet connection' ]
  s.version     = "3.0.0"
  s.author      = "Róbert Selvek"
  s.email       = "me@sellweek.eu"
  s.homepage    = "http://github.com/sellweek/netrand"
  s.required_ruby_version = '>=1.9'
  s.files       = Dir['**/**']
  s.executables = [ 'netrand' ]
  s.test_files  = Dir["test/test*.rb"]
  s.has_rdoc    = true
end