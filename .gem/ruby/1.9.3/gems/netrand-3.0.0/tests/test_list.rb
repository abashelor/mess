require "netrand"
require "test/unit"
class TestList < MiniTest::Unit::TestCase
  def setup
    Netrand.randomize_file_lines("#{File.dirname(__FILE__)}/ZoznamOkresov.txt")
  end
  def test_file_randomization
    original   = Array.new
    randomized = Array.new
    line = 0
    File.open("#{File.dirname(__FILE__)}/ZoznamOkresov.txt", "r") do |f|
      until line == nil
        line = f.gets
        original << line
      end
    end
    
    File.open("#{File.dirname(__FILE__)}/ZoznamOkresov-randomized.txt", "r") do |f|
      until line == nil
        line = f.gets
        randomized << line
      end
    end
    refute_equal(randomized, original)
  end
  
  def test_file_not_same
    rand1 = Array.new
    rand2 = Array.new
    line = 0
    File.open("#{File.dirname(__FILE__)}/ZoznamOkresov-randomized.txt", "r") do |f|
      until line == nil
        line = f.gets
        rand1 << line
      end
    end
    
    File.open("#{File.dirname(__FILE__)}/ZoznamOkresov-randomized.txt", "r") do |f|
      until line == nil
        line = f.gets
        rand2 << line
      end
    end
    refute_equal(rand1, rand2)
  end
  
  
end