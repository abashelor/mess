require "netrand"
require "test/unit"
class TestSequence < MiniTest::Unit::TestCase
  def test_randomization
    ref = (1..953).to_a
    refute_equal(ref, Netrand.sequence(953, 1))
  end
  
  def test_not_same
    nums1 = Netrand.sequence(10)
    nums2 = Netrand.sequence(10)
    refute_equal(nums1, nums2)
  end
  
  def test_error_handling
    assert_raises(ArgumentError) {Netrand.sequence(3, 10)}
    assert_raises(ArgumentError) {Netrand.sequence(0)}
  end
  
end