require "netrand"
require "test/unit"
class TestDice < MiniTest::Unit::TestCase
  def test_real_dice
    Netrand.dice(20).each {|n| assert n<=6}
  end
end