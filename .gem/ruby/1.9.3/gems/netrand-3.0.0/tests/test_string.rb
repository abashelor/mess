require "netrand"
require "test/unit"
class TestString < MiniTest::Unit::TestCase
  def test_not_same
     str1 = Netrand.string(10, 8, :upperalpha, :loweralpha)
     str2 = Netrand.string(10, 8, :upperalpha, :loweralpha)
     refute_equal(str1, str2)
   end
   
   def test_uniq
     str = Netrand.string(10, 8, :loweralpha)
     assert_equal(str.uniq.length, str.length)
   end
   
  def test_error_handling
    assert_raises(ArgumentError) {Netrand.string(10, 0, :digits, :upperalpha)}
    assert_raises(ArgumentError) {Netrand.string(10, 8)}
  end  
  
  def test_digits
    s1 = Netrand.string(10, 8, :digits)
    s1.each do |s|
      s.each_char do |c|
        assert(c.to_i.integer?, "#{c} in #{s} not a number!")
      end
    end
  end
  
  def test_upcase
    s1 = Netrand.string(10, 8, :upperalpha)
    s2 = s1.map {|s| s.upcase}
    assert_equal(s2, s1)
  end
  
  def test_downcase
    s1 = Netrand.string(10, 8, :loweralpha)
    s2 = s1.map {|s| s.downcase}
    assert_equal(s2, s1)
  end
  

  
end