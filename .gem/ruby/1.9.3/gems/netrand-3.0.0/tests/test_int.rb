require "netrand"
require "test/unit"
class TestInt < MiniTest::Unit::TestCase
  
  def test_not_same
    nums1 = Netrand.int(5, 100)
    nums2 = Netrand.int(5, 100)
    refute_equal(nums1, nums2)
  end
  
  def test_error_handling
    assert_raises(ArgumentError) {Netrand.int(0, 10)}
    assert_raises(ArgumentError) {Netrand.int(100, 0)}
    assert_raises(ArgumentError) {Netrand.int(100, 999999, 9999991)}
  end
  
  def test_uniq
    nums = Netrand.int(150, 10000, 1, :unique)
    assert_equal(nums.uniq.length, nums.length)
    assert_equal(150, nums.length)
  end

end