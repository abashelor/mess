require_relative "common"
module Netrand
  
  #Returns sequence of numbers from min to max in random order.
  def self.sequence(max, min = 1)
    if max == 0 then raise ArgumentError, "Maximum number must be bigger than 0" end
    if min > max then raise ArgumentError, "max must be bigger than min!" end
    self.check_quota
    order = self.harvest("http://www.random.org/sequences/?min=#{min}&max=#{max}&col=1&format=plain&rnd=new")
  end
end