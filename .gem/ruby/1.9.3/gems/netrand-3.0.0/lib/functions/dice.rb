require_relative "int"
module Netrand
  # Simulates dice rolls.
  # Example: <br>
  # Netrand.dice(10) <br>
  # Can return array like this: [2, 4, 6, 3, 1, 5, 3, 2] <br>
  def self.dice(dices)
    self.int(dices, 6)
  end
end