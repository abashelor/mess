require "open-uri"
module Netrand
  #Generates exception if quota of random.org is exhausted.
  
  def self.check_quota
    quota = nil
    open("http://www.random.org/quota/?format=plain") do |c|
      quota = c.read.to_i
      self.check_quota_value(quota)
    end
    quota
  end
  
  private
  
  class QuotaExhaustedException < RuntimeError; end
  
  #Harvests lines in plaintext output (private)
  def self.harvest(address)
    values = []
    open(address) do |c|
      c.each_line {|l| values << l.chop.to_i}
    end
    values
  end
  
  def self.check_quota_value(quota)
    if quota < 0 then raise QuotaExhaustedException, "Random.org quota exhausted, please try again after 0:00 UTC" end
  end

end