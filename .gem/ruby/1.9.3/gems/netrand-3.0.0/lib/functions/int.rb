require_relative "common"
module Netrand
  
  #Generates numget random numbers using random.org service.
  #You must set maximum and can set minimum number. 
  #Returns Array of numbers.
  #If you use :unique flag, then there won't be two same numbers in the array.
  
  def self.int(numget, max, min = 1, uniq = :notuniq)
    raise ArgumentError, "You must ask for more numbers than 0" if numget == 0
    raise ArgumentError, "Maximum number must be bigger than 0" if max == 0
    raise ArgumentError, "max must be bigger than min!"         if min > max
    self.check_quota
    values = self.harvest("http://www.random.org/integers/?num=#{numget}&min=#{min}&max=#{max}&format=plain&col=1&base=10")
    if uniq == :unique then self.get_unique_ints(numget, max, min, values) end
    values
  end
  
  private
  
  #Gets unique integers in a way that is fast and conserves bandwidth (private)
  def self.get_unique_ints(numget, max, min, values = [])
    values.uniq!
    while values.length != numget do
      values << self.harvest("http://www.random.org/integers/?num=#{numget - values.length}&min=#{min}&max=#{max}&format=plain&col=1&base=10")
      values.uniq!
    end
    values
  end
  
end