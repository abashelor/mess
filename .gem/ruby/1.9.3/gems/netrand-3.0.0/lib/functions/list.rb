require_relative "sequence"
module Netrand
  
  #Randomizes order of items in array
  #Array must be shorter than 10 000 items
  #Example: <br>
  #Netrand.list([1, 2, 3]) <br>
  #May return [2, 1, 3] 
 
  def self.list(arr)
    randarr = []
    random_positions = self.sequence(arr.length - 1, 0)
    arr = arr.to_enum
    random_positions.each do |pos|
      randarr[pos] = arr.next
    end
    randarr
  end
  
 #Randomizes order of lines in file
 #They are saved to file called input_file_name-randomized.type
 
  def self.randomize_file_lines(filename)
    src = File.new(filename, "r")
    out = File.new(assemble_randomized_path(src), "w")
    values = src.readlines
    randomized_values = self.list(values)
    randomized_values.each {|value| out.write(value)}
    src.close
    out.close
  end
  
  private
  
  #Counts lines in file
  def self.countlines(file_to_count)
    count = 0
    File.open(file_to_count, "r") do |f| 
      ret = 0
      until ret == nil
        ret = f.gets
        count += 1
      end
    end
    count
  end
  
  #Assembles the filename, so that it won't corrupt paths
  def self.assemble_randomized_path(input_file)
    path            = input_file.path
    dir             = File.dirname(path)
    
    filename        = path.split("/")[-1]
    filename        = filename.split(".") 
    filename        = filename[0, filename.length - 1]
    
    filetype        = File.extname(path)
    fn_with_suffix  = filename[0] + "-randomized"
    return dir + "/" + fn_with_suffix + filetype
  end

end