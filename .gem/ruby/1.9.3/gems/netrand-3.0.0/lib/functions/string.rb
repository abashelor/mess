require_relative "common"
module Netrand
  
  #Generates num random strings of len length.
  #They can contain digits, uppercase and lowercase letters.
  #They can be set to be unique.
  #Example:<br>
  #Netrand.string(10, 5, :upperalpha, :loweralpha, :digits, :unique) <br>
  #Will produce 10 5 characters long strings containing uppercase, lowercase letters and digits. 
  #There won't be 2 same characters in one string. This example used all of the possible options
  
  def self.string(num, len, *opts)
    digits      = opts.include?(:digits)      ? "on" : "off"
    upperalpha  = opts.include?(:upperalpha)  ? "on" : "off"
    loweralpha  = opts.include?(:loweralpha)  ? "on" : "off"
    unique      = opts.include?(:unique)      ? "on" : "off"
    if len == 0 then raise ArgumentError, "Strings must be longer than 0 characters." end
    if digits == "off" && upperalpha == "off" && loweralpha == "off" then raise ArgumentError, "You disabled every type of character." end
    values = Array.new
    self.check_quota
    open("http://www.random.org/strings/?num=#{num}&len=#{len}&digits=#{digits}&upperalpha=#{upperalpha}&loweralpha=#{loweralpha}&unique=#{unique}&format=plain") do |c|
      c.each_line {|l| values << l.chop}
    end
    values
  end
end