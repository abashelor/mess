Wrapper around Random.org random number and string generator.  
Pretty much every parameter has maximum of 10 000.  
For more info look in the RDocs.  
install using *sudo gem install Netrand*   
Remember, that the tests only test the module, not Random.org service.   
[Information about Random.org](http://www.random.org/faq/ "Random.org FAQ")