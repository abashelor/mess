U:RDoc::NormalClass[iI"DirectoryWatcher:EF@I"Object;Fo:RDoc::Markup::Document:@parts[o;;[bS:RDoc::Markup::Heading:
leveli:	textI"Synopsis;To:RDoc::Markup::BlankLine o:RDoc::Markup::Paragraph;[I"MA class for watching files within a directory and generating events when;TI"those files change.;T@S;	;
i;I"Details;T@o;;[I"GA directory watcher is an +Observable+ object that sends events to;TI"Mregistered observers when file changes are detected within the directory;TI"being watched.;T@o;;[	I"NThe directory watcher operates by scanning the directory at some interval;TI"Kand creating a list of the files it finds. File events are detected by;TI"Ncomparing the current file list with the file list from the previous scan;TI"Ninterval. Three types of events are supported -- *added*, *modified*, and;To:RDoc::Markup::Verbatim;[I"removed*.
;F:@format0o;;[I"JAn added event is generated when the file appears in the current file;TI"Mlist but not in the previous scan interval file list. A removed event is;TI"Lgenerated when the file appears in the previous scan interval file list;TI"Mbut not in the current file list. A modified event is generated when the;TI"Mfile appears in the current and the previous interval file list, but the;TI"Kfile modification time or the file size differs between the two lists.;T@o;;[I"NThe file events are collected into an array, and all registered observers;TI"Oreceive all file events for each scan interval. It is up to the individual;TI";observers to filter the events they are interested in.;T@S;	;
i;I"File Selection;T@o;;[I"NThe directory watcher uses glob patterns to select the files to scan. The;TI"Kdefault glob pattern will select all regular files in the directory of;TI"interest '*'.;T@o;;[I")Here are a few useful glob examples:;T@o;;[I"@   '*'               => all files in the current directory
;FI"=   '**  '            => all files in all subdirectories
;FI",   '   *.rb'         => all ruby files
;FI"5   'ext   /*.{h,c}'  => all C source code files
;FI"
;FI"NNote*: file events will never be generated for directories. Only regular
;F;0o;;[I")files are included in the file scan.;T@S;	;
i;I"Stable Files;T@o;;[I"GA fourth file event is supported but not enabled by default -- the;To;;[I"Kstable* event. This event is generated after a file has been added or
;F;0o;;[I"Emodified and then remains unchanged for a certain number of scan;TI"intervals.;T@o;;[	I"FTo enable the generation of this event the +stable+ count must be;TI"Hconfigured. This is the number of scan intervals a file must remain;TI"Nunchanged (based modification time and file size) before it is considered;TI"stable.;T@o;;[I"ETo disable this event the +stable+ count should be set to +nil+.;T@S;	;
i;I"
Usage;T@o;;[I"KLearn by Doing -- here are a few different ways to configure and use a;TI"directory watcher.;T@S;	;
i;I"
Basic;T@o;;[I"HThis basic recipe will watch all files in the current directory and;TI"Ngenerate the three default events. We'll register an observer that simply;TI"'prints the events to standard out.;T@o;;[I"!require 'directory_watcher'
;FI"
;FI"#dw = DirectoryWatcher.new '.'
;FI">dw.add_observer {|*args| args.each {|event| puts event}}
;FI"
;FI"dw.start
;FI"Fgets      # when the user hits "enter" the script will terminate
;FI"dw.stop
;F;0S;	;
i;I"$Suppress Initial "added" Events;T@o;;[	I"HThis little twist will suppress the initial "added" events that are;TI"Ggenerated the first time the directory is scanned. This is done by;TI"Opre-loading the watcher with files -- i.e. telling the watcher to scan for;TI"2files before actually starting the scan loop.;T@o;;[I"!require 'directory_watcher'
;FI"
;FI"6dw = DirectoryWatcher.new '.', :pre_load => true
;FI"dw.glob = '   *.rb'
;FI">dw.add_observer {|*args| args.each {|event| puts event}}
;FI"
;FI"dw.start
;FI"Fgets      # when the user hits "enter" the script will terminate
;FI"dw.stop
;F;0o;;[I"LThere is one catch with this recipe. The glob pattern must be specified;TI"Ibefore the pre-load takes place. The glob pattern can be given as an;TI"option to the constructor:;T@o;;[I"Jdw = DirectoryWatcher.new '.', :glob => '**/*.rb', :pre_load => true
;F;0o;;[I"1The other option is to use the reset method:;T@o;;[	I"#dw = DirectoryWatcher.new '.'
;FI"dw.glob = '**/*.rb'
;FI"Hdw.reset true     # the +true+ flag causes the watcher to pre-load
;FI"#                  # the files
;F;0S;	;
i;I"Generate "stable" Events;T@o;;[	I"OIn order to generate stable events, the stable count must be specified. In;TI"Lthis example the interval is set to 5.0 seconds and the stable count is;TI"Mset to 2. Stable events will only be generated for files after they have;TI"/remain unchanged for 10 seconds (5.0 * 2).;T@o;;[I"!require 'directory_watcher'
;FI"
;FI"7dw = DirectoryWatcher.new '.', :glob => '**/*.rb'
;FI"dw.interval = 5.0
;FI"dw.stable = 2
;FI">dw.add_observer {|*args| args.each {|event| puts event}}
;FI"
;FI"dw.start
;FI"Fgets      # when the user hits "enter" the script will terminate
;FI"dw.stop
;F;0S;	;
i;I"Persisting State;T@o;;[	I"LA directory watcher can be configured to persist its current state to a;TI"Jfile when it is stopped and to load state from that same file when it;TI"Gstarts. Setting the +persist+ value to a filename will enable this;TI"feature.;T@o;;[I"!require 'directory_watcher'
;FI"
;FI"7dw = DirectoryWatcher.new '.', :glob => '**/*.rb'
;FI"dw.interval = 5.0
;FI"!dw.persist = "dw_state.yml"
;FI">dw.add_observer {|*args| args.each {|event| puts event}}
;FI"
;FI"/dw.start  # loads state from dw_state.yml
;FI"Fgets      # when the user hits "enter" the script will terminate
;FI".dw.stop   # stores state to dw_state.yml
;F;0S;	;
i;I"Running Once;T@o;;[I"MInstead of using the built in run loop, the directory watcher can be run;TI"None or many times using the +run_once+ method. The state of the directory;TI"4watcher can be loaded and dumped if so desired.;T@o;;[I"7dw = DirectoryWatcher.new '.', :glob => '**/*.rb'
;FI"!dw.persist = "dw_state.yml"
;FI">dw.add_observer {|*args| args.each {|event| puts event}}
;FI"
;FI"4dw.load!       # loads state from dw_state.yml
;FI"dw.run_once
;FI"sleep 5.0
;FI"dw.run_once
;FI"3dw.persist!    # stores state to dw_state.yml
;F;0S;	;
i;I"Scanning Strategies;T@o;;[
I"MBy default DirectoryWatcher uses a thread that scans the directory being;TI"Mwatched for files and calls "stat" on each file. The stat information is;TI"Kused to determine which files have been modified, added, removed, etc.;TI"MThis approach is fairly intensive for short intervals and/or directories;TI"with many files.;T@o;;[	I"JDirectoryWatcher supports using Cool.io, EventMachine, or Rev instead;TI"Oof a busy polling thread. These libraries use system level kernel hooks to;TI"Mreceive notifications of file system changes. This makes DirectoryWorker;TI"much more efficient.;T@o;;[I"BThis example will use Cool.io to generate file notifications.;T@o;;[I"Ldw = DirectoryWatcher.new '.', :glob => '**/*.rb', :scanner => :coolio
;FI">dw.add_observer {|*args| args.each {|event| puts event}}
;FI"
;FI"dw.start
;FI"Fgets      # when the user hits "enter" the script will terminate
;FI"dw.stop
;F;0o;;[I"FThe scanner cannot be changed after the DirectoryWatcher has been;TI"Fcreated. To use an EventMachine scanner, pass :em as the :scanner;TI"option.;T@o;;[I"OIf you wish to use the Cool.io scanner, then you must have the Cool.io gem;TI"Oinstalled. The same goes for EventMachine and Rev. To install any of these;TI"0gems run the following on the command line:;T@o;;[I"gem install cool.io
;FI"gem install eventmachine
;FI"gem install rev
;F;0o;;[I"ONote: Rev has been replace by Cool.io and support for the Rev scanner will;TI"1eventually be dropped from DirectoryWatcher.;T@S;	;
i;I"Contact;T@o;;[I"GA lot of discussion happens about Ruby in general on the ruby-talk;TI"Hmailing list (http://www.ruby-lang.org/en/ml.html), and you can ask;TI"Gany questions you might have there. I monitor the list, as do many;TI"Fother helpful Rubyists, and you're sure to get a quick answer. Of;TI"Hcourse, you're also welcome to email me (Tim Pease) directly at the;TI"Aat tim.pease@gmail.com, and I'll do my best to help you out.;T@o;;[I"G(the above paragraph was blatantly stolen from Nathaniel Talbott's;TI"Test::Unit documentation);T@S;	;
i;I"Author;T@o;;[I"Tim Pease;T:
@fileI"lib/directory_watcher.rb;T;0[[
I"persist;FI"R;F:publicF@[[I"
Event;Fo;;[o;;[I"NAn +Event+ structure contains the _type_ of the event and the file _path_;TI"Gto which the event pertains. The type can be one of the following:;T@o;;[
I"::added      =>  file has been added to the directory
;FI"J:modified   =>  file has been modified (either mtime or size or both
;FI"#                have changed)
;FI">:removed    =>  file has been removed from the directory
;FI"F:stable     =>  file has stabilized since being added or modified;F;0;@@[ [[I"
class;F[[;[	[I"libpath;F@[I"new;F@[I"	path;F@[I"version;F@[:protected[ [:private[ [I"instance;F[[;[[I"add_observer;F@[I"count_observers;F@[I"delete_observer;F@[I"delete_observers;F@[I"	glob;F@[I"
glob=;F@[I"interval;F@[I"interval=;F@[I"	join;F@[I"
load!;F@[I"persist!;F@[I"persist=;F@[I"
reset;F@[I"run_once;F@[I"running?;F@[I"stable;F@[I"stable=;F@[I"
start;F@[I"	stop;F@[;[ [;[ 