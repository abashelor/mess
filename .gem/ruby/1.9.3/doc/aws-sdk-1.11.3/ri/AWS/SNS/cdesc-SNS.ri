U:RDoc::NormalClass[iI"SNS:EFI"AWS::SNS;FI"Object;Fo:RDoc::Markup::Document:@parts[o;;[ :
@fileI"lib/aws/sns/client.rb;To;;[ ;	I"lib/aws/sns/errors.rb;To;;[ ;	I"'lib/aws/sns/has_delivery_policy.rb;To;;[ ;	I"lib/aws/sns/policy.rb;To;;[ ;	I"lib/aws/sns/request.rb;To;;[ ;	I" lib/aws/sns/subscription.rb;To;;[ ;	I"+lib/aws/sns/subscription_collection.rb;To;;[ ;	I"lib/aws/sns/topic.rb;To;;[ ;	I"$lib/aws/sns/topic_collection.rb;To;;[ ;	I"1lib/aws/sns/topic_subscription_collection.rb;To;;[o:RDoc::Markup::Paragraph;[I"DThis class is the starting point for working with Amazon Simple;TI" Notification Service (SNS).;To:RDoc::Markup::BlankLine o;
;[I"QTo use Amazon SNS you must first [sign up here](http://aws.amazon.com/sns/).;T@0o;
;[I"+For more information about Amazon SNS:;T@0o:RDoc::Markup::List:
@type:BULLET:@items[o:RDoc::Markup::ListItem:@label0;[o;
;[I"-[Amazon SNS](http://aws.amazon.com/sns/);To;;0;[o;
;[I"I[Amazon SNS Documentation](http://aws.amazon.com/documentation/sns/);T@0o;
;[I"# Credentials;T@0o;
;[I"?You can setup default credentials for all AWS services via;TI"AWS.config:;T@0o:RDoc::Markup::Verbatim;[I"AWS.config(
;FI"/  :access_key_id => 'YOUR_ACCESS_KEY_ID',
;FI"7  :secret_access_key => 'YOUR_SECRET_ACCESS_KEY')
;F:@format0o;
;[I"7Or you can set them directly on the SNS interface:;T@0o;;[I"sns = AWS::SNS.new(
;FI"/  :access_key_id => 'YOUR_ACCESS_KEY_ID',
;FI"7  :secret_access_key => 'YOUR_SECRET_ACCESS_KEY')
;F;0o;
;[I"@!attribute [r] client;To;;[I"5@return [Client] the low-level SNS client object;F;0;	I"lib/aws/sns.rb;T;	0[ [ [[I"Core::ServiceInterface;Fo;;[ ;	@]@][[I"
class;F[[:public[ [:protected[ [:private[ [I"instance;F[[;[[I"subscriptions;F@][I"topics;F@][;[ [;[ 