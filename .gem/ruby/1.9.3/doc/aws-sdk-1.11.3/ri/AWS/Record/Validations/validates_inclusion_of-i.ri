U:RDoc::AnyMethod[iI"validates_inclusion_of:EFI"4AWS::Record::Validations#validates_inclusion_of;FF:publico:RDoc::Markup::Document:@parts[o:RDoc::Markup::Paragraph;	[I"KValidates that the attribute value is included in the given enumerable;TI"object.;To:RDoc::Markup::BlankLine o:RDoc::Markup::Verbatim;	[I"5class MultipleChoiceAnswer < AWS::Record::Model
;FI"<  validates_inclusion_of :letter, :in => %w(a b c d e)
;FI"	end
;F:@format0o;
;	[I" ### Multi-Valued Attributes;T@o;
;	[I"JYou may use this with multi-valued attributes the same way you use it;TI"#with single-valued attributes.;T@o;
;	[I"H@overload validates_inclusion_of(*attributes, options = {}, &block);To;;	[I">@param attributes A list of attribute names to validate.
;FI"@param [Hash] options
;FI"H@option options [required, Enumerable] :in An enumerable object to
;FI"  check for the value in.
;FI"L@option options [String] :message A custom error message.  The default
;FI"4  `:message` is "is not included in the list".
;FI"I@option options [Boolean] :allow_nil (false) Skip validation if the
;FI"!  attribute value is `nil`.
;FI"K@option options [Boolean] :allow_blank (false) Skip validation if the
;FI"#  attribute value is `blank`.
;FI"G@option options [Symbol] :on (:save) When this validation is run.
;FI"  Valid values include:
;FI"  * `:save`
;FI"  * `:create`
;FI"  * `:update`
;FI"I@option options [Symbol,String,Proc] :if Specifies a method or proc
;FI"H  to call.  The validation will only be run if the return value is
;FI"C  of the method/proc is true (e.g. `:if => :name_changed?` or
;FI"1  `:if => lambda{|book| book.in_stock? }`).
;FI"H@option options [Symbol,String,Proc] :unless Specifies a method or
;FI"K  proc to call.  The validation will *not* be run if the return value
;FI"&  is of the method/proc is false.;F;0:
@fileI""lib/aws/record/validations.rb;T00[ I"(*attributes);F@7