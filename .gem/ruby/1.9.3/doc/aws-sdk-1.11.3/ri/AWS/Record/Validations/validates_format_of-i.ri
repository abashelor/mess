U:RDoc::AnyMethod[iI"validates_format_of:EFI"1AWS::Record::Validations#validates_format_of;FF:publico:RDoc::Markup::Document:@parts[o:RDoc::Markup::Paragraph;	[I"IValidates the attribute's value matches the given regular exression.;To:RDoc::Markup::BlankLine o:RDoc::Markup::Verbatim;	[I"3validates_format_of :year, :with => /^\d{4}$/
;F:@format0o;
;	[I"JYou can also perform a not-match using `:without` instead of `:with`.;T@o;;	[I"5validates_format_of :username, :without => /\d/
;F;0o;
;	[I" ### Multi-Valued Attributes;T@o;
;	[I"JYou may use this with multi-valued attributes the same way you use it;TI"#with single-valued attributes:;T@o;;	[	I"(class Product < AWS::Record::Model
;FI"'  string_attr :tags, :set => true
;FI"8  validates_format_of :tags, :with => /^\w{2,10}$/
;FI"	end
;F;0o;
;	[I"E@overload validates_format_of(*attributes, options = {}, &block);To;;	[I">@param attributes A list of attribute names to validate.
;FI"@param [Hash] options
;FI"C@option options [Regexp] :with If the value matches the given
;FI"*  regex, an error will not be added.
;FI"F@option options [Regexp] :without If the value matches the given
;FI"&  regex, an error will be added.
;FI")  must match, or an error is added.
;FI"L@option options [String] :message A custom error message.  The default
;FI"$  `:message` is "is reserved".
;FI"I@option options [Boolean] :allow_nil (false) Skip validation if the
;FI"!  attribute value is `nil`.
;FI"K@option options [Boolean] :allow_blank (false) Skip validation if the
;FI"#  attribute value is `blank`.
;FI"G@option options [Symbol] :on (:save) When this validation is run.
;FI"  Valid values include:
;FI"  * `:save`
;FI"  * `:create`
;FI"  * `:update`
;FI"I@option options [Symbol,String,Proc] :if Specifies a method or proc
;FI"H  to call.  The validation will only be run if the return value is
;FI"C  of the method/proc is true (e.g. `:if => :name_changed?` or
;FI"1  `:if => lambda{|book| book.in_stock? }`).
;FI"H@option options [Symbol,String,Proc] :unless Specifies a method or
;FI"K  proc to call.  The validation will *not* be run if the return value
;FI"&  is of the method/proc is false.;F;0:
@fileI""lib/aws/record/validations.rb;T00[ I"(*args);F@C