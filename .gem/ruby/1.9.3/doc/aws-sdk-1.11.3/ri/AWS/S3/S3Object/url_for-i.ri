U:RDoc::AnyMethod[iI"url_for:EFI"AWS::S3::S3Object#url_for;FF:publico:RDoc::Markup::Document:@parts[.o:RDoc::Markup::Paragraph;	[	I"?Generates a presigned URL for an operation on this object.;TI"AThis URL can be used by a regular HTTP client to perform the;TI"?desired operation without credentials and without changing;TI"#the permissions of the object.;To:RDoc::Markup::BlankLine o;
;	[I".@example Generate a url to read an object;T@o:RDoc::Markup::Verbatim;	[I",bucket.objects.myobject.url_for(:read)
;F:@format0o;
;	[I"0@example Generate a url to delete an object;T@o;;	[I".bucket.objects.myobject.url_for(:delete)
;F;0o;
;	[I"=@example Override response headers for reading an object;T@o;;	[I"&object = bucket.objects.myobject
;FI"!url = object.url_for(:read,
;FI"H                     :response_content_type => "application/json")
;F;0o;
;	[I"7@example Generate a url that expires in 10 minutes;T@o;;	[I"?bucket.objects.myobject.url_for(:read, :expires => 10*60)
;F;0o;
;	[I";@param [Symbol, String] method The HTTP verb or object;To;;	[I"=method for which the returned URL will be valid.  Valid
;FI"values:
;FI"
;FI"* `:get` or `:read`
;FI"* `:put` or `:write`
;FI"* `:delete`
;F;0o;
;	[I"E@param [Hash] options Additional options for generating the URL.;T@o;
;	[I"=@option options :expires Sets the expiration time of the;To;;	[
I"@URL; after this time S3 will return an error if the URL is
;FI"=used.  This can be an integer (to specify the number of
;FI"@seconds after the current time), a string (which is parsed
;FI"@as a date using Time#parse), a Time, or a DateTime object.
;FI">This option defaults to one hour after the current time.
;F;0o;
;	[I"C@option options [Boolean] :secure (true) Whether to generate a;To;;	[I"-secure (HTTPS) URL or a plain HTTP url.
;F;0o;
;	[I"+@option options [String] :content_type;TI"*@option options [String] :content_md5;TI"@@option options [String] :endpoint Sets the hostname of the;To;;	[I"endpoint.
;F;0o;
;	[I"9@option options [Integer] :port Sets the port of the;To;;	[I"*endpoint (overrides config.s3_port).
;F;0o;
;	[I"B@option options [Boolean] :force_path_style (false) Indicates;To;;	[I"?whether the generated URL should place the bucket name in
;FI"0the path (true) or as a subdomain (false).
;F;0o;
;	[I"=@option options [String] :response_content_type Sets the;To;;	[I"<Content-Type header of the response when performing an
;FI"#HTTP GET on the returned URL.
;F;0o;
;	[I"A@option options [String] :response_content_language Sets the;To;;	[I"@Content-Language header of the response when performing an
;FI"#HTTP GET on the returned URL.
;F;0o;
;	[I"@@option options [String] :response_expires Sets the Expires;To;;	[I"?header of the response when performing an HTTP GET on the
;FI"returned URL.
;F;0o;
;	[I">@option options [String] :response_cache_control Sets the;To;;	[I"=Cache-Control header of the response when performing an
;FI"#HTTP GET on the returned URL.
;F;0o;
;	[I"@@option options [String] :response_content_disposition Sets;To;;	[I"9the Content-Disposition header of the response when
;FI"1performing an HTTP GET on the returned URL.
;F;0o;
;	[I"A@option options [String] :response_content_encoding Sets the;To;;	[I"@Content-Encoding header of the response when performing an
;FI"#HTTP GET on the returned URL.
;F;0o;
;	[I"$@return [URI::HTTP, URI::HTTPS];T:
@fileI"lib/aws/s3/s3_object.rb;T00[ I"(method, options = {});T@�