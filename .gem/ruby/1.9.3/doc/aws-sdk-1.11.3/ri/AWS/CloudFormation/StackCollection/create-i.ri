U:RDoc::AnyMethod[iI"create:EFI"0AWS::CloudFormation::StackCollection#create;FF:publico:RDoc::Markup::Document:@parts[!o:RDoc::Markup::Paragraph;	[I"Creates a new stack.;To:RDoc::Markup::BlankLine o;
;	[I"6@example Creating a stack with a template string.;T@o:RDoc::Markup::Verbatim;	[I"template = <<-JSON
;FI" {
;FI"3   "AWSTemplateFormatVersion" : "2010-09-09",
;FI",   "Description": "A simple template",
;FI"   "Resources": {
;FI"     "web": {
;FI"*       "Type": "AWS::EC2::Instance",
;FI"       "Properties": {
;FI"(         "ImageId": "ami-41814f28"
;FI"       }
;FI"     }
;FI"
   }
;FI"}
;FI"
JSON
;FI"7stack = cfm.stacks.create('stack-name', template)
;F:@format0o;
;	[I"1@example Creating a stack from an S3 object.;T@o;;	[I"Gtemplate = AWS::S3.new.buckets['templates'].objects['template-1']
;FI"7stack = cfm.stacks.create('stack-name', template)
;F;0o;
;	[I"1@example Creating a stack with 3 parameters.;T@o;;	[*I"template = <<-JSON
;FI"{
;FI"2  "AWSTemplateFormatVersion" : "2010-09-09",
;FI"+  "Description": "A simple template",
;FI"  "Parameters" : {
;FI"    "KeyName" : {
;FI"A      "Description" : "Name of a KeyPair to use with SSH.",
;FI"      "Type" : "String"
;FI"    },
;FI"    "SecurityGroup" : {
;FI"?      "Description" : "The security group to launch in.",
;FI"      "Type" : "String"
;FI"    },
;FI"    "InstanceType" : {
;FI">      "Description" : "The size of instance to launch.",
;FI"      "Type" : "String"
;FI"    }
;FI"
  },
;FI"  "Resources": {
;FI"    "web": {
;FI")      "Type": "AWS::EC2::Instance",
;FI"      "Properties": {
;FI"9        "InstanceType": { "Ref" : "InstanceType" },
;FI"?        "SecurityGroups" : [ {"Ref" : "SecurityGroup"} ],
;FI"/        "KeyName": { "Ref" : "KeyName" },
;FI"'        "ImageId": "ami-41814f28"
;FI"      }
;FI"    }
;FI"	  }
;FI"}
;FI"
JSON
;FI"
;FI"Bstack = cfm.stacks.create('name', template, :parameters => {
;FI"%  'KeyName' => 'key-pair-name',
;FI"1  'SecurityGroup' => 'security-group-name',
;FI"%  'InstanceType' => 'm1.large',
;FI"})
;F;0o;
;	[I"@param [String] stack_name;T@o;
;	[I"I@param [String,URI,S3::S3Object,Object] template The stack template.;To;;	[I"<This may be provided in a number of formats including:
;FI"
;FI"?  * a String, containing the template as a JSON document.
;FI"6  * a URL String pointing to the document in S3.
;FI"6  * a URI object pointing to the document in S3.
;FI"8  * an {S3::S3Object} which contains the template.
;FI"H  * an Object which responds to #to_json and returns the template.
;F;0o;
;	[I"@param [Hash] options;T@o;
;	[I"K@option options [Array<String>] :capabilities The list of capabilities;To;;	[I"Ethat you want to allow in the stack. If your stack contains IAM
;FI"Cresources, you must specify the CAPABILITY_IAM value for this
;FI"2parameter; otherwise, this action returns an
;FI"FInsufficientCapabilities error. IAM resources are the following:
;FI"
;FI"  * AWS::IAM::AccessKey
;FI"  * AWS::IAM::Group
;FI"  * AWS::IAM::Policy
;FI"  * AWS::IAM::User
;FI"'  * AWS::IAM::UserToGroupAddition
;F;0o;
;	[I"8@option options [Boolean] :disable_rollback (false);To;;	[I"ASet to true to disable rollback on stack creation failures.
;F;0o;
;	[I"@@option options [Object] :notify One or more SNS topics ARN;To;;	[I"?string or {SNS::Topic} objects.  This param may be passed
;FI"Cas a single value or as an array. CloudFormation will publish
;FI"+stack related events to these topics.
;F;0o;
;	[I"A@option options [Hash] :parameters A hash that specifies the;To;;	[I"(input parameters of the new stack.
;F;0o;
;	[I"=@option options [Integer] :timeout The number of minutes;To;;	[I"8that may pass before the stack creation fails.  If
;FI"B`:disable_rollback` is false, the stack will be rolled back.
;F;0o;
;	[I"@return [Stack];T:
@fileI"0lib/aws/cloud_formation/stack_collection.rb;T00[ I")(stack_name, template, options = {});F@�