U:RDoc::AnyMethod[iI"update:EFI"&AWS::CloudFormation::Stack#update;FF:publico:RDoc::Markup::Document:@parts[o:RDoc::Markup::Paragraph;	[I"@param [Hash] options;To:RDoc::Markup::BlankLine o;
;	[I"?@option options [String,URI,S3::S3Object,Object] :template;To:RDoc::Markup::Verbatim;	[I"HA new stack template.  This may be provided in a number of formats
;FI"including:
;FI"
;FI"?  * a String, containing the template as a JSON document.
;FI"6  * a URL String pointing to the document in S3.
;FI"6  * a URI object pointing to the document in S3.
;FI"8  * an {S3::S3Object} which contains the template.
;FI"H  * an Object which responds to #to_json and returns the template.
;F:@format0o;
;	[I"A@option options [Hash] :parameters A hash that specifies the;To;;	[I"(input parameters of the new stack.
;F;0o;
;	[I"J@option options[Array<String>] :capabilities The list of capabilities;To;;	[I"Ethat you want to allow in the stack. If your stack contains IAM
;FI"Cresources, you must specify the CAPABILITY_IAM value for this
;FI"2parameter; otherwise, this action returns an
;FI"FInsufficientCapabilities error. IAM resources are the following:
;FI"
;FI"  * AWS::IAM::AccessKey
;FI"  * AWS::IAM::Group
;FI"  * AWS::IAM::Policy
;FI"  * AWS::IAM::User
;FI"'  * AWS::IAM::UserToGroupAddition
;F;0o;
;	[I"@return [nil];T:
@fileI"%lib/aws/cloud_formation/stack.rb;T00[ I"(options = {});F@4