U:RDoc::AnyMethod[iI"send_email:EFI"'AWS::SimpleEmailService#send_email;FF:publico:RDoc::Markup::Document:@parts[ o:RDoc::Markup::Paragraph;	[I"Sends an email.;To:RDoc::Markup::BlankLine o:RDoc::Markup::Verbatim;	[I"ses.send_email(
;FI"%  :subject => 'A Sample Email',
;FI"  :to => 'john@doe.com',
;FI"   :from => 'no@reply.com',
;FI"(  :body_text => 'sample text ...',
;FI"/  :body_html => '<p>sample text ...</p>')
;F:@format0o;
;	[I"EYou can also pass multiple email addresses for the `:to`, `:cc`,;TI"A`:bcc` and `:reply_to` options.  Email addresses can also be;TI"formatted with names.;T@o;;	[
I"ses.send_email(
;FI"%  :subject => 'A Sample Email',
;FI"J  :to => ['"John Doe" <john@doe.com>', '"Jane Doe" <jane@doe.com>'],
;FI"   :from => 'no@reply.com',
;FI"(  :body_text => 'sample text ...')
;F;0o;
;	[I"@param [Hash] options;TI"K@option options [required,String] :subject The subject of the message.;To;;	[I"@A short summary of the content, which will appear in the #
;FI"recipient's inbox.
;F;0o;
;	[I"H@option options [required,String] :from The sender's email address.;TI"M@option options [String,Array] :to The address(es) to send the email to.;TI"K@option options [String,Array] :cc The address(es) to cc (carbon copy);To;;	[I"the email to.
;F;0o;
;	[I"F@option options [String,Array] :bcc The address(es) to bcc (blind;To;;	[I" carbon copy) the email to.
;F;0o;
;	[I"L@option options [String,Array] :reply_to The reply-to email address(es);To;;	[I"Dfor the message. If the recipient replies to the message, each
;FI".reply-to address will receive the reply.
;F;0o;
;	[I"E@option options [String] :return_path The email address to which;To;;	[	I"Hbounce notifications are to be forwarded. If the message cannot be
;FI"Hdelivered to the recipient, then an error message will be returned
;FI"Ffrom the recipient's ISP; this message will then be forwarded to
;FI"?the email address specified by the `:return_path` option.
;F;0o;
;	[I"A@option options [String] :body_text The email text contents.;To;;	[I":You must provide `:body_text`, `:body_html` or both.
;F;0o;
;	[I"A@option options [String] :body_html The email html contents.;To;;	[I":You must provide `:body_text`, `:body_html` or both.
;F;0o;
;	[I"G@option options [String] :subject_charset The character set of the;To;;	[I"H`:subject` string.  If the text must contain any other characters,
;FI"Dthen you must also specify the character set. Examples include
;FI"@UTF-8, ISO-8859-1, and Shift_JIS. Defaults to 7-bit ASCII.
;F;0o;
;	[I"I@option options [String] :body_text_charset The character set of the;To;;	[I"J`:body_text` string.  If the text must contain any other characters,
;FI"Dthen you must also specify the character set. Examples include
;FI"@UTF-8, ISO-8859-1, and Shift_JIS. Defaults to 7-bit ASCII.
;F;0o;
;	[I"I@option options [String] :body_html_charset The character set of the;To;;	[I"J`:body_html` string.  If the text must contain any other characters,
;FI"Dthen you must also specify the character set. Examples include
;FI"@UTF-8, ISO-8859-1, and Shift_JIS. Defaults to 7-bit ASCII.
;F;0o;
;	[I"(@option options [String] :body_html;TI"@return [nil];T:
@fileI"$lib/aws/simple_email_service.rb;T00[ I"(options = {});F@q