U:RDoc::NormalClass[iI"SimpleEmailService:EFI"AWS::SimpleEmailService;FI"Object;Fo:RDoc::Markup::Document:@parts[o;;[ :
@fileI"+lib/aws/simple_email_service/client.rb;To;;[ ;	I"=lib/aws/simple_email_service/email_address_collection.rb;To;;[ ;	I"+lib/aws/simple_email_service/errors.rb;To;;[ ;	I"-lib/aws/simple_email_service/identity.rb;To;;[ ;	I"8lib/aws/simple_email_service/identity_collection.rb;To;;[ ;	I"+lib/aws/simple_email_service/quotas.rb;To;;[ ;	I",lib/aws/simple_email_service/request.rb;To;;[Ro:RDoc::Markup::Paragraph;[I"=This class is the starting point for working with Amazon;TI"SimpleEmailService (SES).;To:RDoc::Markup::BlankLine o;
;[I"4To use Amazon SimpleEmailService you must first;TI"/[sign up here](http://aws.amazon.com/ses/);T@'o;
;[I":For more information about Amazon SimpleEmailService:;T@'o:RDoc::Markup::List:
@type:BULLET:@items[o:RDoc::Markup::ListItem:@label0;[o;
;[I"<[Amazon SimpleEmailService](http://aws.amazon.com/ses/);To;;0;[o;
;[I"X[Amazon SimpleEmailService Documentation](http://aws.amazon.com/documentation/ses/);T@'o;
;[I"# Credentials;T@'o;
;[I"?You can setup default credentials for all AWS services via;TI"AWS.config:;T@'o:RDoc::Markup::Verbatim;[I"AWS.config(
;FI"/  :access_key_id => 'YOUR_ACCESS_KEY_ID',
;FI"7  :secret_access_key => 'YOUR_SECRET_ACCESS_KEY')
;F:@format0o;
;[I"FOr you can set them directly on the SimpleEmailService interface:;T@'o;;[I"(ses = AWS::SimpleEmailService.new(
;FI"/  :access_key_id => 'YOUR_ACCESS_KEY_ID',
;FI"7  :secret_access_key => 'YOUR_SECRET_ACCESS_KEY')
;F;0o;
;[I"# Rails;T@'o;
;[I"IIf you want to use Amazon SimpleEmailService to send email from your;TI"4Rails application you just need to do 2 things:;T@'o;;:NUMBER;[o;;0;[o;
;[I"5Configure your AWS credentials with {AWS.config};To;;0;[o;
;[I"$Set SES as the delivery method:;T@'o;;[I"8config.action_mailer.delivery_method = :amazon_ses
;F;0o;
;[I"<This has only been tested with Rails 2.3 and Rails 3.0.;T@'o;
;[I"# Identities;T@'o;
;[
I"KBefore you can send emails, you need to verify one or more identities.;TI"OIdentities are email addresses or domain names that you have control over.;TI"�Until you have [requested production access](http://docs.amazonwebservices.com/ses/latest/DeveloperGuide/InitialSetup.Customer.html);TI"Nyou will only be able to send emails to and from verified email addresses;TI"and domains.;T@'o;
;[I"!## Verifying Email Addresses;T@'o;
;[I"GYou can verify an email address for sending/receiving emails using;TI"the identities collection.;T@'o;;[I">identity = ses.identities.verify('email@yourdomain.com')
;FI""identity.verified? #=> false
;F;0o;
;[I"GYou will be sent an email address with a link.  Follow the link to;TI"verify the email address.;T@'o;
;[I"## Verifying Domains;T@'o;
;[I"KYou can also verify an entire domain for sending and receiving emails.;T@'o;;[I"8identity = ses.identities.verify('yourdomain.com')
;FI"!identity.verification_token
;FI"8#=> "216D+lZbhUL0zOoAkC83/0TAl5lJSzLmzsOjtXM7AeM="
;F;0o;
;[I"HYou will be expected to update the DNS records for your domain with;TI"Ethe given verification token.  See the service documentation for;TI"more details.;T@'o;
;[I"## Listing Identities;T@'o;
;[I"&You can enumerate all identities:;T@'o;;[I"$ses.identities.map(&:identity)
;FI"-#=> ['email@foo.com', 'somedomain.com']
;F;0o;
;[I"7You can filter the types of identities enumerated:;T@'o;;[I"6domains = ses.identities.domains.map(&:identity)
;FI"Femail_addresses = ses.identities.email_addresses.map(&:identity)
;F;0o;
;[I"KYou can get the verification status and token from identities as well.;T@'o;;[I"# for an email address
;FI";identity = ses.identities['youremail@yourdomain.com']
;FI"'identity.verified? #=> true/false
;FI"
;FI"# for a domain
;FI"1identity = ses.identities['yourdomain.com']
;FI"'identity.verified? #=> true/false
;FI"+identity.verification_token #=> '...'
;F;0o;
;[I"# Sending Email;T@'o;
;[I"5To send a basic email you can use {#send_email}.;T@'o;;[I"ses.send_email(
;FI"%  :subject => 'A Sample Email',
;FI"%  :from => 'sender@domain.com',
;FI"'  :to => 'receipient@domain.com',
;FI"+  :body_text => 'Sample email text.',
;FI".  :body_html => '<h1>Sample Email</h1>')
;F;0o;
;[I"KIf you need to send email with attachments or have other special needs;TI"Dthat send_email does not support you can use {#send_raw_email}.;T@'o;;[I"!ses.send_raw_email(<<EMAIL)
;FI"Subject: A Sample Email
;FI"From: sender@domain.com
;FI"To: receipient@domain.com
;FI"
;FI"Sample email text.
;FI"EMAIL
;F;0o;
;[I"EIf you prefer, you can also set the sender and recipient in ruby;TI"when sending raw emails:;T@'o;;[
I"Oses.send_raw_email(<<EMAIL, :to => 'to@foo.com', :from => 'from@foo.com')
;FI"Subject: A Sample Email
;FI"
;FI"Sample email text.
;FI"EMAIL
;F;0o;
;[I"# Quotas;T@'o;
;[I"KBased on several factors, Amazon SES determines how much email you can;TI"Ksend and how quickly you can send it. These sending limits are defined;TI"as follows:;T@'o;;;;[o;;0;[o;
;[I"I`:max_send_rate` - Maximum number of emails you can send per second.;To;;0;[o;
;[I"E`:max_24_hour_send` - Maximum number of emails you can send in a;TI"24-hour period.;T@'o;
;[I"NTo get your current quotas (and how many emails you have sent in the last;TI"24 hours):;T@'o;;[I"ses.quotas
;FI"Q# => {:max_24_hour_send=>200, :max_send_rate=>1.0, :sent_last_24_hours=>22}
;F;0o;
;[I"# Statistics;T@'o;
;[I"4You can get statistics about individual emails:;T@'o;;[I"$ses.statistics.each do |stats|
;FI"$  puts "Sent: #{stats[:sent]}"
;FI">  puts "Delivery Attempts: #{stats[:delivery_attempts]}"
;FI"*  puts "Rejects: #{stats[:rejects]}"
;FI"*  puts "Bounces: #{stats[:bounces]}"
;FI"0  puts "Complaints: #{stats[:complaints]}"
;FI"	end
;F;0o;
;[I"@!attribute [r] client;To;;[I"D@return [Client] the low-level SimpleEmailService client object;F;0;	I"$lib/aws/simple_email_service.rb;T;	0[ [ [[I"Core::ServiceInterface;Fo;;[ ;	@@[[I"
class;F[[:public[ [:protected[ [:private[ [I"instance;F[[;[[I"deliver;F@[I"deliver!;F@[I"email_addresses;F@[I"identities;F@[I"quotas;F@[I"send_email;F@[I"send_raw_email;F@[I"settings;F@[I"statistics;F@[;[[I"nest_options;F@[I"require_each;F@[I"require_one_of;F@[;[ 