U:RDoc::NormalClass[iI"AutoScaling:EFI"AWS::AutoScaling;FI"Object;Fo:RDoc::Markup::Document:@parts[o;;[ :
@fileI"%lib/aws/auto_scaling/activity.rb;To;;[ ;	I"0lib/aws/auto_scaling/activity_collection.rb;To;;[ ;	I"#lib/aws/auto_scaling/client.rb;To;;[ ;	I"#lib/aws/auto_scaling/errors.rb;To;;[ ;	I""lib/aws/auto_scaling/group.rb;To;;[ ;	I"-lib/aws/auto_scaling/group_collection.rb;To;;[ ;	I"*lib/aws/auto_scaling/group_options.rb;To;;[ ;	I"%lib/aws/auto_scaling/instance.rb;To;;[ ;	I"0lib/aws/auto_scaling/instance_collection.rb;To;;[ ;	I"1lib/aws/auto_scaling/launch_configuration.rb;To;;[ ;	I"<lib/aws/auto_scaling/launch_configuration_collection.rb;To;;[ ;	I"7lib/aws/auto_scaling/notification_configuration.rb;To;;[ ;	I"Blib/aws/auto_scaling/notification_configuration_collection.rb;To;;[ ;	I"$lib/aws/auto_scaling/request.rb;To;;[ ;	I"+lib/aws/auto_scaling/scaling_policy.rb;To;;[ ;	I"6lib/aws/auto_scaling/scaling_policy_collection.rb;To;;[ ;	I"3lib/aws/auto_scaling/scaling_policy_options.rb;To;;[ ;	I"-lib/aws/auto_scaling/scheduled_action.rb;To;;[ ;	I"8lib/aws/auto_scaling/scheduled_action_collection.rb;To;;[ ;	I" lib/aws/auto_scaling/tag.rb;To;;[ ;	I"+lib/aws/auto_scaling/tag_collection.rb;To;;[#o:RDoc::Markup::Paragraph;[I"DThis class is the starting point for working with Auto Scaling.;To:RDoc::Markup::BlankLine o;
;[I"'To use Auto Scaling you must first;TI"8[sign up here](http://aws.amazon.com/autoscaling/).;T@Po;
;[I"-For more information about Auto Scaling:;T@Po:RDoc::Markup::List:
@type:BULLET:@items[o:RDoc::Markup::ListItem:@label0;[o;
;[I"7[Auto Scaling](http://aws.amazon.com/autoscaling/);To;;0;[o;
;[I"S[Auto Scaling Documentation](http://aws.amazon.com/documentation/autoscaling/);T@Po;
;[I"# Credentials;T@Po;
;[I"?You can setup default credentials for all AWS services via;TI"AWS.config:;T@Po:RDoc::Markup::Verbatim;[I"AWS.config(
;FI"/  :access_key_id => 'YOUR_ACCESS_KEY_ID',
;FI"7  :secret_access_key => 'YOUR_SECRET_ACCESS_KEY')
;F:@format0o;
;[I"DOr you can set them directly on the AWS::AutoScaling interface:;T@Po;;[I"*auto_scaling = AWS::AutoScaling.new(
;FI"/  :access_key_id => 'YOUR_ACCESS_KEY_ID',
;FI"7  :secret_access_key => 'YOUR_SECRET_ACCESS_KEY')
;F;0o;
;[I"# Launch Configurations;T@Po;
;[I"DYou need to create a launch configuration before you can create;TI"an Auto Scaling Group.;T@Po;;[I"1# needs a name, image id, and instance type
;FI"@launch_config = auto_scaling.launch_configurations.create(
;FI"6  'launch-config-name', 'ami-12345', 'm1.small')
;F;0o;
;[I"BIf you have previously created a launch configuration you can;TI"9reference using the {LaunchConfigurationCollection}.;T@Po;;[I"Nlaunch_config = auto_scaling.launch_configurations['launch-config-name']
;F;0o;
;[I"# Auto Scaling Groups;T@Po;
;[I"NGiven a launch configuration, you can now create an Auto Scaling {Group}.;T@Po;;[
I"6group = auto_scaling.groups.create('group-name',
;FI"/  :launch_configuration => launch_config,
;FI"9  :availability_zones => %w(us-west-2a us-west-2b),
;FI"  :min_size => 1,
;FI"  :max_size => 4)
;F;0o;
;[I"@!attribute [r] client;To;;[I"=@return [Client] the low-level AutoScaling client object;F;0;	I"lib/aws/auto_scaling.rb;T;	0[ [ [[I"Core::ServiceInterface;Fo;;[ ;	@�@�[[I"
class;F[[:public[ [:protected[ [:private[ [I"instance;F[[;[[I"activities;F@�[I"adjustment_types;F@�[I"groups;F@�[I"instances;F@�[I"launch_configurations;F@�[I"$metric_collection_granularities;F@�[I"metric_collection_types;F@�[I" notification_configurations;F@�[I"notification_types;F@�[I"scaling_process_types;F@�[I"scheduled_actions;F@�[I"	tags;F@�[;[ [;[ 