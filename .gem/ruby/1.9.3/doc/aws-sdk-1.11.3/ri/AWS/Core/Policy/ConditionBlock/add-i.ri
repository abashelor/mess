U:RDoc::AnyMethod[iI"add:EFI"*AWS::Core::Policy::ConditionBlock#add;FF:publico:RDoc::Markup::Document:@parts[o:RDoc::Markup::Paragraph;	[I":Adds a condition to the block.  This method defines a;TI"?convenient set of abbreviations for operators based on the;TI"+type of value passed in.  For example:;To:RDoc::Markup::BlankLine o:RDoc::Markup::Verbatim;	[I"2conditions.add(:is, :secure_transport, true)
;F:@format0o;
;	[I"Maps to:;T@o;;	[I"1{ "Bool": { "aws:SecureTransport": true } }
;F;0o;
;	[I"While:;T@o;;	[I"0conditions.add(:is, :s3_prefix, "photos/")
;F;0o;
;	[I"Maps to:;T@o;;	[I"4{ "StringEquals": { "s3:prefix": "photos/" } }
;F;0o;
;	[I"=The following list shows which operators are accepted as;TI"=symbols and how they are represented in the JSON policy:;T@o:RDoc::Markup::List:
@type:BULLET:@items[o:RDoc::Markup::ListItem:@label0;	[o;
;	[I"=`:is` (StringEquals, NumericEquals, DateEquals, or Bool);To;;0;	[o;
;	[I"`:like` (StringLike);To;;0;	[o;
;	[I" `:not_like` (StringNotLike);To;;0;	[o;
;	[I"A`:not` (StringNotEquals, NumericNotEquals, or DateNotEquals);To;;0;	[o;
;	[I"C`:greater_than`, `:gt` (NumericGreaterThan or DateGreaterThan);To;;0;	[o;
;	[I"#`:greater_than_equals`, `:gte`;TI"8(NumericGreaterThanEquals or DateGreaterThanEquals);To;;0;	[o;
;	[I":`:less_than`, `:lt` (NumericLessThan or DateLessThan);To;;0;	[o;
;	[I" `:less_than_equals`, `:lte`;TI"2(NumericLessThanEquals or DateLessThanEquals);To;;0;	[o;
;	[I"!`:is_ip_address` (IpAddress);To;;0;	[o;
;	[I"%`:not_ip_address` (NotIpAddress);To;;0;	[o;
;	[I"`:is_arn` (ArnEquals);To;;0;	[o;
;	[I"`:not_arn` (ArnNotEquals);To;;0;	[o;
;	[I"`:is_arn_like` (ArnLike);To;;0;	[o;
;	[I"!`:not_arn_like` (ArnNotLike);T@o;
;	[I"<@param [Symbol or String] operator The operator used to;To;;	[I":compare the key with the value.  See above for valid
;FI"'values and their interpretations.
;F;0o;
;	[I">@param [Symbol or String] key The key to compare.  Symbol;To;;	[I"6keys are inflected to match AWS conventions.  By
;FI"4default, the key is assumed to be in the "aws"
;FI"=namespace, but if you prefix the symbol name with "s3_"
;FI":it will be sent in the "s3" namespace.  For example,
;FI"/`:s3_prefix` is sent as "s3:prefix" while
;FI";`:secure_transport` is sent as "aws:SecureTransport".
;FI"	See
;FI"Vhttp://docs.amazonwebservices.com/AmazonS3/latest/dev/UsingResOpsConditions.html
;FI"=for a list of the available keys for each action in S3.
;F;0o;
;	[I"8@param [Mixed] values The value to compare against.;To;;	[I"This can be:
;FI"* a String
;FI"* a number
;FI"!* a Date, DateTime, or Time
;FI"* a boolean value
;FI">This method does not attempt to validate that the values
;FI"<are valid for the operators or keys they are used with.;F;0:
@fileI"lib/aws/core/policy.rb;T00[ I"(operator, key, *values);T@�