U:RDoc::AnyMethod[iI"create:EFI"&AWS::EC2::VolumeCollection#create;FF:publico:RDoc::Markup::Document:@parts[o:RDoc::Markup::Paragraph;	[I"ACreates a new Amazon EBS volume that any Amazon EC2 instance;TI":in the same Availability Zone can attach to. For more;TI"žinformation about Amazon EBS, go to the [Amazon Elastic Compute Cloud User Guide](http://docs.amazonwebservices.com/AWSEC2/latest/UserGuide/index.html?using-ebs.html).;To:RDoc::Markup::BlankLine o;
;	[I"<@return [Volume] An object representing the new volume.;T@o;
;	[I";@param [Hash] options Options for creating the volume.;To:RDoc::Markup::Verbatim;	[I">`:availability_zone` and one of `:size`, `:snapshot`, or
;FI"!`:snapshot_id` is required.
;F:@format0o;
;	[I"?@option options [Integer] :size The size of the volume, in;To;;	[I"7GiBs.  Valid values: 1 - 1024.  If `:snapshot` or
;FI"?`:snapshot_id` is specified, this defaults to the size of
;FI"the specified snapshot.
;F;0o;
;	[I"D@option options [Snapshot] :snapshot The snapshot from which to;To;;	[I"create the new volume.
;F;0o;
;	[I"A@option options [String] :snapshot_id The ID of the snapshot;To;;	[I"*from which to create the new volume.
;F;0o;
;	[I"B@option options [String, AvailabilityZone] :availability_zone;To;;	[I">The Availability Zone in which to create the new volume.
;FI">To get a list of the availability zones you can use, see
;FI"{EC2#availability_zones}.
;F;0o;
;	[I"#@option options [String] :iops;T@o;
;	[I"*@option options [String] :volume_type;T@o;
;	[I"@return [Volume];T:
@fileI"%lib/aws/ec2/volume_collection.rb;T00[ I"(options = {});F@@