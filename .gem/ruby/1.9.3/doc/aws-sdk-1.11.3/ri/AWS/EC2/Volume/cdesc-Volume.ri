U:RDoc::NormalClass[iI"Volume:EFI"AWS::EC2::Volume;FI"AWS::EC2::Resource;Fo:RDoc::Markup::Document:@parts[o;;[o:RDoc::Markup::Paragraph;[I"%Represents an Amazon EBS volume.;To:RDoc::Markup::BlankLine o;	;[I"G@example Create an empty 15GiB volume and attach it to an instance;To:RDoc::Markup::Verbatim;[	I".volume = ec2.volumes.create(:size => 15,
;FI"E                            :availability_zone => "us-west-2a")
;FI"Gattachment = volume.attach_to(ec2.instances["i-123"], "/dev/sdf")
;FI"3sleep 1 until attachment.status != :attaching
;F:@format0o;	;[I"E@example Remove all attachments from a volume and then delete it;To;;[
I"-volume.attachments.each do |attachment|
;FI")  attachment.delete(:force => true)
;FI"	end
;FI"/sleep 1 until volume.status == :available
;FI"volume.delete
;F;0o;	;[I";@attr_reader [Symbol] status The status of the volume.;To;;[I"Possible values:
;FI"
;FI"  * `:creating`
;FI"  * `:available`
;FI"  * `:in_use`
;FI"  * `:deleting`
;FI"  * `:deleted`
;FI"  * `:error`
;F;0o;	;[I":@attr_reader [Integer] size The size of the volume in;To;;[I"gigabytes.
;F;0o;	;[I"=@attr_reader [String] availability_zone_name Name of the;To;;[I"8Availability Zone in which the volume was created.
;F;0o;	;[I"A@attr_reader [Time] create_time The time at which the volume;To;;[I"was created.
;F;0o;	;[I"@attr_reader [String] iops;T:
@fileI"lib/aws/ec2/volume.rb;T;0[[
I"id;FI"R;F:publicF@G[ [[I"TaggedItem;Fo;;[ ;@G@G[[I"
class;F[[;[[I"new;F@G[:protected[ [:private[ [I"instance;F[[;[[I"attach;F@G[I"attach_to;F@G[I"attachments;F@G[I"availability_zone;F@G[I"create_snapshot;F@G[I"delete;F@G[I"detach_from;F@G[I"exists?;F@G[I"snapshot;F@G[;[ [;[ 