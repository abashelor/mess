U:RDoc::NormalClass[iI"LoadBalancer:EFI"AWS::ELB::LoadBalancer;FI"AWS::Core::Resource;Fo:RDoc::Markup::Document:@parts[o;;[o:RDoc::Markup::Paragraph;[I">@attr_reader [String] name The name of the load balancer.;To:RDoc::Markup::BlankLine o;	;[I"M@attr_reader [Array<String>] availability_zone_names Return the names of;To:RDoc::Markup::Verbatim;[I"Bthe availability zones this load balancer routes traffic to.
;F:@format0o;	;[I"J@attr_reader [String] canonical_hosted_zone_name Provides the name of;To;;[I"Fthe Amazon Route 53 hosted zone that is associated with the load
;FI"�balancer.  For more information: [using-domain-names-with-elb.html](http://docs.amazonwebservices.com/ElasticLoadBalancing/latest/DeveloperGuide/index.html?using-domain-names-with-elb.html).
;F;0o;	;[I"K@attr_reader [String] canonical_hosted_zone_name_id Provides the ID of;To;;[I"Fthe Amazon Route 53 hosted zone name that is associated with the
;FI"�load balancer.  For more information: [using-domain-names-with-elb.html](http://docs.amazonwebservices.com/ElasticLoadBalancing/latest/DeveloperGuide/index.html?using-domain-names-with-elb.html).
;F;0o;	;[I"C@attr_reader [String] dns_name Specifies the external DNS name;To;;[I")associated with this load balancer.
;F;0o;	;[I">@attr_reader [Hash] policy_descriptions Returns a hash of;To;;[I"I`:app_cookie_stickiness_policies`, `:lb_cookie_stickiness_policies`
;FI"3and `:other_policies`.  See also {#policies}.
;F;0o;	;[I"I@attr_reader [String,nil] scheme Specifies the type of LoadBalancer.;To;;[
I"MThis attribute it set only for LoadBalancers attached to an Amazon VPC.
;FI"IIf the Scheme is 'internet-facing', the LoadBalancer has a publicly
;FI"?resolvable DNS name that resolves to public IP addresses.
;FI"BIf the Scheme is 'internal', the LoadBalancer has a publicly
;FI"@resolvable DNS name that resolves to private IP addresses.
;F;0o;	;[I"N@attr_reader [Array<String>] subnet_ids Provides a list of VPC subnet IDs;To;;[I"for the LoadBalancer.
;F;0o;	;[I"%@attr_reader [Hash] health_check;To;;[I"BReturns a hash of the various health probes conducted on the
;FI"Cload balancer instances.  The following entries are returned:
;FI"* `:healthy_threshold`
;FI"* `:unhealthy_threshold`
;FI"* `:interval`
;FI"* `:target`
;FI"* `:timeout`
;FI"HSee {#configure_health_check} for more details on what each of the
;FI" configuration values mean.
;F;0o;	;[I"@return [Hash];T:
@fileI"!lib/aws/elb/load_balancer.rb;T;0[ [ [ [[I"
class;F[[:public[[I"new;F@T[:protected[ [:private[ [I"instance;F[[;[[I"availability_zones;F@T[I"backend_server_policies;F@T[I"configure_health_check;F@T[I"delete;F@T[I"exists?;F@T[I"instances;F@T[I"listeners;F@T[I"policies;F@T[I"security_groups;F@T[I"source_security_group;F@T[I"subnets;F@T[;[[I"get_resource;F@T[I"resource_identifiers;F@T[;[ 