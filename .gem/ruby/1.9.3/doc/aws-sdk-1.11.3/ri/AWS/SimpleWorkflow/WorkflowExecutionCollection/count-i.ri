U:RDoc::AnyMethod[iI"
count:EFI";AWS::SimpleWorkflow::WorkflowExecutionCollection#count;FF:publico:RDoc::Markup::Document:@parts[ o:RDoc::Markup::Paragraph;	[I"EReturns the number of workflow executions within the domain that;TI"Dmeet the specified filtering criteria.  Counts can be truncated;TI"*so you should check the return value.;To:RDoc::Markup::BlankLine o:RDoc::Markup::Verbatim;	[I".count = domain.workflow_executions.count
;FI"<puts(count.truncated? ? "#{count.to_i}+" : count.to_i)
;F:@format0o;
;	[I":@note You may only pass one of the following options:;To;;	[I"4`:workflow_id`, `:workflow_type`, `:tagged` or
;FI"G`:status` with a "closed" value (`:status` with `:open` is okay).
;F;0o;
;	[I"H@note This operation is eventually consistent. The results are best;To;;	[I"Deffort and may not exactly reflect recent updates and changes.
;F;0o;
;	[I"@param [Hash] options;T@o;
;	[I"H@option options [Symbol] :status Filters workflow executions by the;To;;	[I"Bgiven status.  If status is not provided then it defaults to
;FI"D`:open` unless you pass `:closed_between` (then it defaults to
;FI"`:closed`).
;FI"
;FI"@If `:status` is anything besides `:open` or `:closed` then
;FI"Cit may not be passed with `:workflow_id`, `:workflow_type` or
;FI"`:tagged`.
;FI"
;FI",Accepted values for `:status` include:
;FI"
;FI"* `:open`
;FI"* `:closed`
;FI"* `:completed`
;FI"* `:failed`
;FI"* `:canceled`
;FI"* `:terminated`
;FI"* `:continued`
;FI"* `:timed_out`
;F;0o;
;	[I"F@option options [Time] :started_after Filters workflow executions;To;;	[	I"1down to those started after the given time.
;FI"
;FI"HYou may pass `:started_after` with `:started_before`, but not with
;FI"*`:closed_after` or `:closed_before`.
;F;0o;
;	[I"G@option options [Time] :started_before Filters workflow executions;To;;	[	I"2down to those started before the given time.
;FI"
;FI"HYou may pass `:started_after` with `:started_before`, but not with
;FI"*`:closed_after` or `:closed_before`.
;F;0o;
;	[I"E@option options [Time] :closed_after Filters workflow executions;To;;	[I"+to those closed after the given time.
;FI"
;FI"H* You may pass `:closed_after` with `:closed_before`, but not with
;FI".  `:started_after` or `:started_before`.
;FI"
;FI"H* This option is invalid when counting or listing open executions.
;F;0o;
;	[I"F@option options [Time] :closed_before Filters workflow executions;To;;	[I",to those closed before the given time.
;FI"
;FI"H* You may pass `:closed_after` with `:closed_before`, but not with
;FI".  `:started_after` or `:started_before`.
;FI"
;FI"H* This option is invalid when counting or listing open executions.
;F;0o;
;	[I"G@option options [String] :workflow_id (nil) If specified, workflow;To;;	[I":executions are filtered by the provided workflow id.
;F;0o;
;	[I"G@option options [String] :tagged (nil) Filters workflow executions;To;;	[I"by the given tag.
;F;0o;
;	[I"=@option options [WorkflowType,Hash] :workflow_type (nil);To;;	[I"?Filters workflow executions with the given workflow type.
;FI"D`:workflow_type` can be a {WorkflowType} object or a hash with
;FI"-a workflow type `:name` and `:version`.
;F;0o;
;	[I":@return [Count] Returns a possibly truncated count of;To;;	[I"workflow executions.;F;0:
@fileI"=lib/aws/simple_workflow/workflow_execution_collection.rb;T00[ I"(options = {});F@~