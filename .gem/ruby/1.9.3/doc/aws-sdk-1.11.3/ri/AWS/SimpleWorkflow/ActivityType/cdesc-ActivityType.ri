U:RDoc::NormalClass[iI"ActivityType:EFI"&AWS::SimpleWorkflow::ActivityType;FI"	Type;Fo:RDoc::Markup::Document:@parts[o;;[&o:RDoc::Markup::Paragraph;[I"### Registering an ActivityType;To:RDoc::Markup::BlankLine o;	;[I"KTo register an activity type you should use the #activity_types method;TI"on the domain:;T@o:RDoc::Markup::Verbatim;[I"@domain.activity_types.register('name', 'version', { ... })
;F:@format0o;	;[I"JSee {ActivityTypeCollection#register} for a complete list of options.;T@o;	;[I"$## Deprecating an activity type;T@o;	;[I"JActivityType inherits from the generic {Type} base class.  Defined in;TI"/{Type} are a few useful methods including:;T@o:RDoc::Markup::List:
@type:BULLET:@items[o:RDoc::Markup::ListItem:@label0;[o;	;[I"{Type#deprecate};To;;0;[o;	;[I"{Type#deprecated?};T@o;	;[I"5You can use these to deprecate an activity type:;T@o;;[I"7domain.activity_types['name','version'].deprecate
;F;0o;	;[I"M@attr_reader [Time] creation_date When the workflow type was registered.;T@o;	;[I"D@attr_reader [Time,nil] deprecation_date When the workflow type;To;;[I"Jwas deprecated, or nil if the workflow type has not been deprecated.
;F;0o;	;[I"K@attr_reader [String,nil] description The description of this workflow;To;;[I"9type, or nil if was not set when it was registered.
;F;0o;	;[I"H@attr_reader [Symbol] status The status of this workflow type.  The;To;;[I";status will either be `:registered` or `:deprecated`.
;F;0o;	;[I"D@attr_reader [Integer,:none,nil] default_task_heartbeat_timeout;To;;[I"FThe default maximum time specified when registering the activity
;FI"?type, before which a worker processing a task must report
;FI"@progress. If the timeout is exceeded, the activity task is
;FI"Bautomatically timed out. If the worker subsequently attempts
;FI"Cto record a heartbeat or return a result, it will be ignored.
;FI"
;FI"AThe return value may be an integer (number of seconds), the
;FI"Dsymbol `:none` (implying no timeout) or `nil` (not specified).
;F;0o;	;[I"0@attr_reader [String,nil] default_task_list;To;;[I"?The default task list specified for this activity type at
;FI"Dregistration. This default task list is used if a task list is
;FI",not provided when a task is scheduled.
;F;0o;	;[I"L@attr_reader [Integer,:none,nil] default_task_schedule_to_close_timeout;To;;[I"AThe default maximum duration specified when registering the
;FI"Factivity type, for tasks of this activity type. You can override
;FI"*this default when scheduling a task.
;FI"
;FI"AThe return value may be an integer (number of seconds), the
;FI"Dsymbol `:none` (implying no timeout) or `nil` (not specified).
;F;0o;	;[I"L@attr_reader [Integer,:none,nil] default_task_schedule_to_start_timeout;To;;[I"FThe optional default maximum duration specified when registering
;FI"Athe activity type, that a task of an activity type can wait
;FI"(before being assigned to a worker.
;FI"
;FI"AThe return value may be an integer (number of seconds), the
;FI"Dsymbol `:none` (implying no timeout) or `nil` (not specified).
;F;0o;	;[I"I@attr_reader [Integer,:none,nil] default_task_start_to_close_timeout;To;;[	I"CThe default maximum duration for activity tasks of this type.
;FI"
;FI"AThe return value may be an integer (number of seconds), the
;FI"Csymbol `:none` (implying no timeout) or `nil` (not specified).;F;0:
@fileI"-lib/aws/simple_workflow/activity_type.rb;T;0[ [ [ [[I"
class;F[[:public[ [:protected[ [:private[ [I"instance;F[[;[ [;[ [;[ 