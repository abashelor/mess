U:RDoc::NormalClass[iI"CloudWatch:EFI"AWS::CloudWatch;FI"Object;Fo:RDoc::Markup::Document:@parts[o;;[ :
@fileI"!lib/aws/cloud_watch/alarm.rb;To;;[ ;	I",lib/aws/cloud_watch/alarm_collection.rb;To;;[ ;	I".lib/aws/cloud_watch/alarm_history_item.rb;To;;[ ;	I"9lib/aws/cloud_watch/alarm_history_item_collection.rb;To;;[ ;	I""lib/aws/cloud_watch/client.rb;To;;[ ;	I""lib/aws/cloud_watch/errors.rb;To;;[ ;	I""lib/aws/cloud_watch/metric.rb;To;;[ ;	I"3lib/aws/cloud_watch/metric_alarm_collection.rb;To;;[ ;	I"-lib/aws/cloud_watch/metric_collection.rb;To;;[ ;	I"-lib/aws/cloud_watch/metric_statistics.rb;To;;[ ;	I"#lib/aws/cloud_watch/request.rb;To;;[o:RDoc::Markup::Paragraph;[I"IThis class is the starting point for working with Amazon CloudWatch.;To:RDoc::Markup::BlankLine o;
;[I",To use Amazon CloudWatch you must first;TI"7[sign up here](http://aws.amazon.com/cloudwatch/).;T@2o;
;[I"2For more information about Amazon CloudWatch:;T@2o:RDoc::Markup::List:
@type:BULLET:@items[o:RDoc::Markup::ListItem:@label0;[o;
;[I";[Amazon CloudWatch](http://aws.amazon.com/cloudwatch/);To;;0;[o;
;[I"W[Amazon CloudWatch Documentation](http://aws.amazon.com/documentation/cloudwatch/);T@2o;
;[I"# Credentials;T@2o;
;[I"?You can setup default credentials for all AWS services via;TI"AWS.config:;T@2o:RDoc::Markup::Verbatim;[I"AWS.config(
;FI"/  :access_key_id => 'YOUR_ACCESS_KEY_ID',
;FI"7  :secret_access_key => 'YOUR_SECRET_ACCESS_KEY')
;F:@format0o;
;[I"COr you can set them directly on the AWS::CloudWatch interface:;T@2o;;[I"cw = AWS::CloudWatch.new(
;FI"/  :access_key_id => 'YOUR_ACCESS_KEY_ID',
;FI"7  :secret_access_key => 'YOUR_SECRET_ACCESS_KEY')
;F;0o;
;[I"# Using the Client;T@2o;
;[	I"QAWS::CloudWatch does not provide higher level abstractions for CloudWatch at;TI"Bthis time.  You can still access all of the API methods using;TI"K{AWS::CloudWatch::Client}.  Here is how you access the client and make;TI"a simple request:;T@2o;;[I"cw = AWS::CloudWatch.new
;FI"
;FI"&resp = cw.client.describe_alarms
;FI"*resp[:metric_alarms].each do |alarm|
;FI"  puts alarm[:alarm_name]
;FI"	end
;F;0o;
;[I"GSee {Client} for documentation on all of the supported operations.;T@2o;
;[I"@!attribute [r] client;To;;[I"<@return [Client] the low-level CloudWatch client object;F;0;	I"lib/aws/cloud_watch.rb;T;	0[ [ [[I"Core::ServiceInterface;Fo;;[ ;	@t@t[[I"
class;F[[:public[ [:protected[ [:private[ [I"instance;F[[;[	[I"alarm_history_items;F@t[I"alarms;F@t[I"metrics;F@t[I"put_metric_data;F@t[;[ [;[ 