U:RDoc::AnyMethod[iI"	each:EFI"'AWS::SimpleDB::ItemCollection#each;FF:publico:RDoc::Markup::Document:@parts[%o:RDoc::Markup::Paragraph;	[I">Yields to the block once for each item in the collection.;TI"/This method can yield two type of objects:;To:RDoc::Markup::BlankLine o:RDoc::Markup::List:
@type:BULLET:@items[o:RDoc::Markup::ListItem:@label0;	[o;
;	[I"BAWS::SimpleDB::Item objects (only the item name is populated);To;;0;	[o;
;	[I"GAWS::SimpleDB::ItemData objects (some or all attributes populated);T@o;
;	[I"HThe default mode of an ItemCollection is to yield Item objects with;TI"no populated attributes.;T@o:RDoc::Markup::Verbatim;	[
I".# only receives item names from SimpleDB
;FI"!domain.items.each do |item|
;FI"  puts item.name
;FI"5  puts item.class.name # => AWS::SimpleDB::Item
;FI"	end
;F:@format0o;
;	[I"CYou can switch a collection into yielded {ItemData} objects by;TI"+specifying what attributes to request:;T@o;;	[	I"3domain.items.select(:all).each do |item_data|
;FI">  puts item_data.class.name # => AWS::SimpleDB::ItemData
;FI"K  puts item_data.attributes # => { 'attr-name' => 'attr-value', ... }
;FI"	end
;F;0o;
;	[I"CYou can also pass the standard scope options to #each as well:;T@o;;	[	I"<# output the item names of the 10 most expensive items
;FI"Odomain.items.each(:order => [:price, :desc], :limit => 10).each do |item|
;FI"  puts item.name
;FI"	end
;F;0o;
;	[I"?@yield [item] Yields once for every item in the {#domain}.;T@o;
;	[I"E@yieldparam [Item,ItemData] item If the item collection has been;To;;	[	I"Escoped by chaining `#select` or by passing the `:select` option
;FI"Ethen {ItemData} objects (that contain a hash of attributes) are
;FI"Ayielded.  If no list of attributes has been provided, then#
;FI":{Item} objects (with no populated data) are yielded.
;F;0o;
;	[I"@param options [Hash];T@o;
;	[I"C@option options [Boolean] :consistent_read (false) Causes this;To;;	[I":method to yield the most current data in the domain.
;F;0o;
;	[I"E@option options [Mixed] :select If select is provided, then each;To;;	[I"<will yield {ItemData} objects instead of empty {Item}.
;FI""The `:select` option may be:
;FI"
;FI"@* `:all` - Specifies that all attributes should requested.
;FI"
;FI"E* A single or array of attribute names (as strings or symbols).
;FI";  This causes the named attribute(s) to be requested.
;F;0o;
;	[I"?@option options :where Restricts the item collection using;To;;	[I".{#where} before querying (see {#where}).
;F;0o;
;	[I"@@option options :order Changes the order in which the items;To;;	[I"%will be yielded (see {#order}).
;F;0o;
;	[I";@option options :limit [Integer] The maximum number of;To;;	[I"#items to fetch from SimpleDB.
;F;0o;
;	[I"F@option options :batch_size Specifies a maximum number of records;To;;	[I"Fto fetch from SimpleDB in a single request.  SimpleDB may return
;FI"?fewer items than :batch_size per request, but never more.
;FI";Generally you should not need to specify this option.
;F;0o;
;	[I"D@return [String,nil] Returns a next token that can be used with;To;;	[	I"Dthe exact same SimpleDB select expression to get more results.
;FI"?A next token is returned ONLY if there was a limit on the
;FI"<expression, otherwise all items will be enumerated and
;FI"nil is returned.;F;0:
@fileI")lib/aws/simple_db/item_collection.rb;T00[ I"(options = {});F@~