U:RDoc::NormalClass[iI"CloudFront:EFI"AWS::CloudFront;FI"Object;Fo:RDoc::Markup::Document:@parts[	o;;[ :
@fileI""lib/aws/cloud_front/client.rb;To;;[ ;	I""lib/aws/cloud_front/errors.rb;To;;[ ;	I"#lib/aws/cloud_front/request.rb;To;;[o:RDoc::Markup::Paragraph;[I"IThis class is the starting point for working with Amazon CloudFront.;To:RDoc::Markup::BlankLine o;
;[I",To use Amazon CloudFront you must first;TI"7[sign up here](http://aws.amazon.com/cloudfront/).;T@o;
;[I"2For more information about Amazon CloudFront:;T@o:RDoc::Markup::List:
@type:BULLET:@items[o:RDoc::Markup::ListItem:@label0;[o;
;[I";[Amazon CloudFront](http://aws.amazon.com/cloudfront/);To;;0;[o;
;[I"W[Amazon CloudFront Documentation](http://aws.amazon.com/documentation/cloudfront/);T@o;
;[I"# Credentials;T@o;
;[I"?You can setup default credentials for all AWS services via;TI"AWS.config:;T@o:RDoc::Markup::Verbatim;[I"AWS.config(
;FI"/  :access_key_id => 'YOUR_ACCESS_KEY_ID',
;FI"7  :secret_access_key => 'YOUR_SECRET_ACCESS_KEY')
;F:@format0o;
;[I"@Or you can set them directly on the AWS::Route53 interface:;T@o;;[I"cf = AWS::CloudFront.new(
;FI"/  :access_key_id => 'YOUR_ACCESS_KEY_ID',
;FI"7  :secret_access_key => 'YOUR_SECRET_ACCESS_KEY')
;F;0o;
;[I"# Using the Client;T@o;
;[	I"QAWS::CloudFront does not provide higher level abstractions for CloudFront at;TI"Bthis time.  You can still access all of the API methods using;TI"K{AWS::CloudFront::Client}.  Here is how you access the client and make;TI"a simple request:;T@o;;[I"   cf = AWS::CloudFront.new
;FI"
;FI")resp = cf.client.list_distributions
;FI")resp[:items].each do |distribution|
;FI"  # ...
;FI"	end
;F;0o;
;[I"GSee {Client} for documentation on all of the supported operations.;T@o;
;[I"@!attribute [r] client;To;;[I"<@return [Client] the low-level CloudFront client object;F;0;	I"lib/aws/cloud_front.rb;T;	0[ [ [[I"Core::ServiceInterface;Fo;;[ ;	@\@\[[I"
class;F[[:public[ [:protected[ [:private[ [I"instance;F[[;[ [;[ [;[ 