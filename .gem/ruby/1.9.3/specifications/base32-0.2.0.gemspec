# -*- encoding: utf-8 -*-

Gem::Specification.new do |s|
  s.name = "base32"
  s.version = "0.2.0"

  s.required_rubygems_version = Gem::Requirement.new(">= 0") if s.respond_to? :required_rubygems_version=
  s.authors = ["Samuel Tesla"]
  s.date = "2011-08-12"
  s.email = "samuel.tesla@gmail.com"
  s.extra_rdoc_files = ["README"]
  s.files = ["README"]
  s.require_paths = ["lib", "lib"]
  s.requirements = ["none"]
  s.rubygems_version = "1.8.23"
  s.summary = "Ruby extension for base32 encoding and decoding"

  if s.respond_to? :specification_version then
    s.specification_version = 3

    if Gem::Version.new(Gem::VERSION) >= Gem::Version.new('1.2.0') then
    else
    end
  else
  end
end
