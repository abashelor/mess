# -*- encoding: utf-8 -*-

Gem::Specification.new do |s|
  s.name = "coco"
  s.version = "0.7.1"

  s.required_rubygems_version = Gem::Requirement.new(">= 0") if s.respond_to? :required_rubygems_version=
  s.authors = ["Xavier Nayrac"]
  s.date = "2013-07-05"
  s.description = "\"Code coverage tool for ruby 1.9.2 to 2.0.\nUse it by \"require 'coco'\" from rspec or unit/test.\nIt display names of uncovered files on console.\nIt builds simple html report.\nIt reports sources that have no tests.\nIt's configurable with a simple yaml file."
  s.email = "xavier.nayrac@gmail.com"
  s.homepage = "http://lkdjiin.github.com/coco/"
  s.licenses = ["GPL-3"]
  s.require_paths = ["lib"]
  s.required_ruby_version = Gem::Requirement.new(">= 1.9.2")
  s.rubygems_version = "1.8.23"
  s.summary = "Code coverage tool for ruby 1.9.2 to 2.0"

  if s.respond_to? :specification_version then
    s.specification_version = 4

    if Gem::Version.new(Gem::VERSION) >= Gem::Version.new('1.2.0') then
    else
    end
  else
  end
end
