# -*- encoding: utf-8 -*-

Gem::Specification.new do |s|
  s.name = "secretsharing"
  s.version = "0.3"

  s.required_rubygems_version = Gem::Requirement.new(">= 0") if s.respond_to? :required_rubygems_version=
  s.authors = ["Alexander Klink"]
  s.date = "2011-06-26"
  s.description = "A libary for sharing secrets in an information-theoretically secure way.\nIt uses Shamir's secret sharing to enable sharing a (random) secret between\nn persons where k <= n persons are enough to recover the secret. k-1 secret\nshare holders learn nothing about the secret when they combine their shares.\n"
  s.email = "secretsharing@alech.de"
  s.extra_rdoc_files = ["README"]
  s.files = ["README"]
  s.require_paths = ["lib"]
  s.rubygems_version = "1.8.23"
  s.summary = "A library to share secrets in an information-theoretically secure way."

  if s.respond_to? :specification_version then
    s.specification_version = 3

    if Gem::Version.new(Gem::VERSION) >= Gem::Version.new('1.2.0') then
    else
    end
  else
  end
end
