# -*- encoding: utf-8 -*-

Gem::Specification.new do |s|
  s.name = "ghi"
  s.version = "0.9.0.20130912"

  s.required_rubygems_version = Gem::Requirement.new(">= 0") if s.respond_to? :required_rubygems_version=
  s.authors = ["Stephen Celis"]
  s.date = "2013-09-12"
  s.description = "GitHub Issues on the command line. Use your `$EDITOR`, not your browser.\n"
  s.email = "stephen@stephencelis.com"
  s.executables = ["ghi"]
  s.files = ["bin/ghi"]
  s.homepage = "https://github.com/stephencelis/ghi"
  s.require_paths = ["lib"]
  s.rubygems_version = "1.8.23"
  s.summary = "GitHub Issues command line interface"

  if s.respond_to? :specification_version then
    s.specification_version = 4

    if Gem::Version.new(Gem::VERSION) >= Gem::Version.new('1.2.0') then
      s.add_development_dependency(%q<rake>, [">= 0"])
      s.add_development_dependency(%q<ronn>, [">= 0"])
    else
      s.add_dependency(%q<rake>, [">= 0"])
      s.add_dependency(%q<ronn>, [">= 0"])
    end
  else
    s.add_dependency(%q<rake>, [">= 0"])
    s.add_dependency(%q<ronn>, [">= 0"])
  end
end
