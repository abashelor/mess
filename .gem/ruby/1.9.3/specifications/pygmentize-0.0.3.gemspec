# -*- encoding: utf-8 -*-

Gem::Specification.new do |s|
  s.name = "pygmentize"
  s.version = "0.0.3"

  s.required_rubygems_version = Gem::Requirement.new(">= 0") if s.respond_to? :required_rubygems_version=
  s.authors = ["Damian Janowski", "Michel Martens"]
  s.date = "2011-05-12"
  s.description = "A Ruby gem that vendors Pygments"
  s.email = ["djanowski@dimaion.com", "michel@soveran.com"]
  s.executables = ["pygmentize"]
  s.files = ["bin/pygmentize"]
  s.homepage = "http://github.com/djanowski/pygmentize"
  s.require_paths = ["lib"]
  s.rubygems_version = "1.8.23"
  s.summary = "A Ruby gem that vendors Pygments"

  if s.respond_to? :specification_version then
    s.specification_version = 3

    if Gem::Version.new(Gem::VERSION) >= Gem::Version.new('1.2.0') then
    else
    end
  else
  end
end
