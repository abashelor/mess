# -*- encoding: utf-8 -*-

Gem::Specification.new do |s|
  s.name = "sprockets-handlebars_template"
  s.version = "1.0.0"

  s.required_rubygems_version = Gem::Requirement.new(">= 0") if s.respond_to? :required_rubygems_version=
  s.authors = ["Guten"]
  s.date = "2012-12-23"
  s.description = "a handlebars template for sprockets.\n"
  s.email = "ywzhaifei@gmail.com"
  s.homepage = "http://github.com/GutenYe/sprockets-handlebars_template"
  s.require_paths = ["lib"]
  s.rubygems_version = "1.8.23"
  s.summary = "a handlebars template for sprockets."

  if s.respond_to? :specification_version then
    s.specification_version = 3

    if Gem::Version.new(Gem::VERSION) >= Gem::Version.new('1.2.0') then
      s.add_runtime_dependency(%q<sprockets>, [">= 0"])
      s.add_runtime_dependency(%q<barber>, [">= 0"])
    else
      s.add_dependency(%q<sprockets>, [">= 0"])
      s.add_dependency(%q<barber>, [">= 0"])
    end
  else
    s.add_dependency(%q<sprockets>, [">= 0"])
    s.add_dependency(%q<barber>, [">= 0"])
  end
end
