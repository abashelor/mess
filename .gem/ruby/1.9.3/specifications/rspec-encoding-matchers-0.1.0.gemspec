# -*- encoding: utf-8 -*-

Gem::Specification.new do |s|
  s.name = "rspec-encoding-matchers"
  s.version = "0.1.0"

  s.required_rubygems_version = Gem::Requirement.new(">= 0") if s.respond_to? :required_rubygems_version=
  s.authors = ["Jeff Pollard"]
  s.date = "2011-02-21"
  s.description = "RSpec matchers for Ruby 1.9 string encodings."
  s.email = ["jeff.pollard@gmail.com"]
  s.homepage = ""
  s.require_paths = ["lib"]
  s.rubyforge_project = "rspec-encoding-matchers"
  s.rubygems_version = "1.8.23"
  s.summary = "RSpec matchers for Ruby 1.9 string encodings."

  if s.respond_to? :specification_version then
    s.specification_version = 3

    if Gem::Version.new(Gem::VERSION) >= Gem::Version.new('1.2.0') then
      s.add_development_dependency(%q<rspec>, [">= 0"])
      s.add_development_dependency(%q<rake>, [">= 0"])
      s.add_development_dependency(%q<ZenTest>, [">= 0"])
    else
      s.add_dependency(%q<rspec>, [">= 0"])
      s.add_dependency(%q<rake>, [">= 0"])
      s.add_dependency(%q<ZenTest>, [">= 0"])
    end
  else
    s.add_dependency(%q<rspec>, [">= 0"])
    s.add_dependency(%q<rake>, [">= 0"])
    s.add_dependency(%q<ZenTest>, [">= 0"])
  end
end
