# -*- encoding: utf-8 -*-

Gem::Specification.new do |s|
  s.name = "less"
  s.version = "1.2.21"

  s.required_rubygems_version = Gem::Requirement.new(">= 0") if s.respond_to? :required_rubygems_version=
  s.authors = ["cloudhead"]
  s.date = "2010-01-13"
  s.description = "LESS is leaner CSS"
  s.email = "self@cloudhead.net"
  s.executables = ["lessc"]
  s.extra_rdoc_files = ["LICENSE", "README.md"]
  s.files = ["bin/lessc", "LICENSE", "README.md"]
  s.homepage = "http://www.lesscss.org"
  s.rdoc_options = ["--charset=UTF-8"]
  s.require_paths = ["lib"]
  s.rubyforge_project = "less"
  s.rubygems_version = "1.8.23"
  s.summary = "LESS compiler"

  if s.respond_to? :specification_version then
    s.specification_version = 3

    if Gem::Version.new(Gem::VERSION) >= Gem::Version.new('1.2.0') then
      s.add_runtime_dependency(%q<treetop>, [">= 1.4.2"])
      s.add_runtime_dependency(%q<mutter>, [">= 0.4.2"])
    else
      s.add_dependency(%q<treetop>, [">= 1.4.2"])
      s.add_dependency(%q<mutter>, [">= 0.4.2"])
    end
  else
    s.add_dependency(%q<treetop>, [">= 1.4.2"])
    s.add_dependency(%q<mutter>, [">= 0.4.2"])
  end
end
