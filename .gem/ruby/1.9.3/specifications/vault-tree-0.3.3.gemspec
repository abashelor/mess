# -*- encoding: utf-8 -*-

Gem::Specification.new do |s|
  s.name = "vault-tree"
  s.version = "0.3.3"

  s.required_rubygems_version = Gem::Requirement.new(">= 0") if s.respond_to? :required_rubygems_version=
  s.authors = ["Andy Bashelor"]
  s.date = "2014-02-01"
  s.description = "Vault Tree is a Ruby library for executing distributed cryptographic contracts."
  s.email = ["abash1212@gmail.com"]
  s.homepage = ""
  s.licenses = ["MIT"]
  s.require_paths = ["lib"]
  s.rubygems_version = "1.8.23"
  s.summary = "The Self Enforcing Contract"

  if s.respond_to? :specification_version then
    s.specification_version = 3

    if Gem::Version.new(Gem::VERSION) >= Gem::Version.new('1.2.0') then
      s.add_runtime_dependency(%q<rbnacl>, ["= 2.0.0"])
      s.add_runtime_dependency(%q<secretsharing>, ["= 0.3"])
      s.add_development_dependency(%q<bundler>, ["~> 1.3"])
      s.add_development_dependency(%q<rspec>, [">= 0"])
      s.add_development_dependency(%q<cucumber>, [">= 0"])
      s.add_development_dependency(%q<relish>, [">= 0"])
    else
      s.add_dependency(%q<rbnacl>, ["= 2.0.0"])
      s.add_dependency(%q<secretsharing>, ["= 0.3"])
      s.add_dependency(%q<bundler>, ["~> 1.3"])
      s.add_dependency(%q<rspec>, [">= 0"])
      s.add_dependency(%q<cucumber>, [">= 0"])
      s.add_dependency(%q<relish>, [">= 0"])
    end
  else
    s.add_dependency(%q<rbnacl>, ["= 2.0.0"])
    s.add_dependency(%q<secretsharing>, ["= 0.3"])
    s.add_dependency(%q<bundler>, ["~> 1.3"])
    s.add_dependency(%q<rspec>, [">= 0"])
    s.add_dependency(%q<cucumber>, [">= 0"])
    s.add_dependency(%q<relish>, [">= 0"])
  end
end
