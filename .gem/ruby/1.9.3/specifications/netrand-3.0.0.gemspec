# -*- encoding: utf-8 -*-

Gem::Specification.new do |s|
  s.name = "netrand"
  s.version = "3.0.0"

  s.required_rubygems_version = Gem::Requirement.new(">= 0") if s.respond_to? :required_rubygems_version=
  s.authors = ["R\u{c3}\u{b3}bert Selvek"]
  s.date = "2011-07-12"
  s.description = "Wrapper around Random.org random number and string generator."
  s.email = "me@sellweek.eu"
  s.executables = ["netrand"]
  s.files = ["bin/netrand"]
  s.homepage = "http://github.com/sellweek/netrand"
  s.require_paths = ["lib"]
  s.required_ruby_version = Gem::Requirement.new(">= 1.9")
  s.requirements = ["Working internet connection"]
  s.rubygems_version = "1.8.23"
  s.summary = "Generate true random numbers using random.org"

  if s.respond_to? :specification_version then
    s.specification_version = 3

    if Gem::Version.new(Gem::VERSION) >= Gem::Version.new('1.2.0') then
    else
    end
  else
  end
end
