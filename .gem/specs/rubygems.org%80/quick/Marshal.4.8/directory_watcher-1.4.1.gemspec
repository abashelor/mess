u:Gem::Specification�["
1.8.6i"directory_watcherU:Gem::Version["
1.4.1u:	Time���    "`A class for watching files within a directory and generating events when those files changeU:Gem::Requirement[[[">=U; ["0U;[[[">=U; ["0"	ruby[	o:Gem::Dependency
:@requirementU;[[[">=U; ["
1.2.4:@prereleaseF:@version_requirements@ :
@name"bones-git:
@type:developmento;
;	U;[[[">=U; ["0;
F;@*;"rev;;o;
;	U;[[[">=U; ["0.12.10;
F;@4;"eventmachine;;o;
;	U;[[[">=U; ["
3.7.0;
F;@>;"
bones;;"directory_watcher"tim.pease@gmail.com["Tim Pease"GThe directory watcher operates by scanning a directory at some interval and
generating a list of files based on a user supplied glob pattern. As the file
list changes from one interval to the next, events are generated and
dispatched to registered observers. Three types of events are supported --
added, modified, and removed."0http://gemcutter.org/gems/directory_watcher0@[ 