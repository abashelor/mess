u:Gem::Specification&[I"1.8.11:ETiI"maruku; TU:Gem::Version[I"
0.6.1; TIu:	Time "�    :@_zoneI"UTC; TI"?Maruku is a Markdown-superset interpreter written in Ruby.; TU:Gem::Requirement[[[I">=; TU;[I"0; TU;	[[[I">=; TU;[I"0; TI"	ruby; F[o:Gem::Dependency
:
@nameI"syntax; T:@requirementU;	[[[I">=; TU;[I"
1.0.0; T:
@type:runtime:@prereleaseF:@version_requirements@"o;

;I"syntax; T;U;	[[[I"~>; TU;[I"
1.0.0; T;:development;F;@,o;

;I"	rake; T;U;	[[[I"~>; TU;[I"
0.9.2; T;;;F;@60I"ben@benhollis.net; T[I"Andrea Censi; TI"Nathan Weizenbaum; TI"�Maruku is a Markdown interpreter in Ruby.
	It features native export to HTML and PDF (via Latex). The
	output is really beautiful!
	; TI"%http://github.com/bhollis/maruku; TT@[I"
GPL-2; T