u:Gem::Specification�["0.8.10i"marukuU:Gem::Version["
0.5.0Iu:	Time���    :@marshal_with_utc_coercionF"#A Markdown interpreter in RubyU:Gem::Requirement[[[">U; ["
0.0.00"	ruby[o:Gem::Dependency:@version_requirement0:
@name"syntax:@version_requirementsU;[[[">=U; ["
1.0.00"andrea@rubyforge.org["Andrea Censi"�Maruku is a Markdown interpreter in Ruby. It features native export to HTML and PDF (via Latex). The output is really beautiful!" http://maruku.rubyforge.orgF@0